<?php
    require_once('include.php');
    $review = htmlspecialchars($_POST['review_text']);
    $rating = $_POST['rating'];
    
    $movie_id = $_GET['movie_id'];

    if($is_logged_in && !$is_disabled){
        $success=ReviewDao::insertReview($user->id, $movie_id, $review, $rating);
        if ($success) {
            header("Location: /movie.php?id=$movie_id");
            die();
        }
        else {
            echo "error in submitting.";
        }
    } else {
        echo "Log In to submit review.";
    }
?>