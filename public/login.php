<?php
require_once('include.php');

$redirect = get($_POST['redirect'], get($_GET['redirect'], '/index.php'));
$action = $_SERVER['SCRIPT_NAME'] . "?redirect=$redirect";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = htmlspecialchars($_POST['username']);
    $pass = $_POST['pass'];
    $user = UserDao::authenticateUser($username, $pass);
    if ($user != null) {
        session_start();
        $_SESSION['user'] = $user;
        header("Location: $redirect");
        die();
    } else {
        $loginFailed = true;
    }
}

if ($is_logged_in) {
    header("Location: $redirect");
    die();
}
require_once('templates/login_page.php')
?>

