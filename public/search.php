<?php
require_once('include.php');
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();

if (isset($_GET['search']) || (isset($_GET['genre']) && $_GET['genre'] != '') || (isset($_GET['lang']) && $_GET['lang'] != '')) {
    $search = get($_GET['search'], '');
    $genre_id = get($_GET['genre'], '');
    $lang_id = get($_GET['lang'], '');
    $sort = (int) get($_GET['sort'], 1);
    $sort = is_int($sort) ? $sort : 1;
    $sort = $sort > 4 || $sort < 0 ? 1 : $sort;
    if ($genre_id != '') {
        if (!isset($genres[$genre_id])) {
            header("Location: /");
            die();
        }
    }
    if ($lang_id != '') {
        if (!isset($languages[$lang_id])) {
            header("Location: /");
            die();
        }
    }
    $page = (int) get($_GET['page'], 1);
    $page = is_int($page) ? $page : 1;
    $page = $page <= 0 ? 1 : $page;
    $movies = MovieDao::search($search, $genre_id, $lang_id, $sort, $page);
} else {
    header("Location: /");
    die();
}
$search_movies_only = true;
$body = 'templates/search_page.php';
require_once('templates/page.php');
?>