<?php
require_once('include.php');

if (!$is_logged_in)
    header("Location: /");

$type = $_GET['type'];

if(!$type) {
    $type = "unseen";
} elseif ($type !== "unseen" && $type !== "seen") {
    $type = "unseen";
}

$notifications = NotificationDao::getNotificationsByUser($user->id, $type);
function getDateTimeString($dt)
{
    $date = date_create_from_format("Y-m-d H:i:s+", $dt);
    return date_format($date, "H:i - d M Y");
}
$body = 'templates/notifications_page.php';
require_once('templates/page.php');
?>