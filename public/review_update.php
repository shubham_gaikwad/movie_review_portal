<?php
    require_once('include.php');
    $id = $_REQUEST['id'];
    if(!$is_logged_in || !isset($id)){
        header("Location: /index.php");
        die();
    }
    $review = ReviewDao::getByID($_GET['id']);
    if(!$review) {
        header("Location: /index.php");
        die();
    }
    if ($review->user_id !== $user->id) {
        header("Location: /index.php");
        die();
    }   
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $review = htmlspecialchars($_POST['review_text']);
        $rating = $_POST['rating'];
        $success=ReviewDao::updateReview($id, $review, $rating);
        if ($success) {
            header("Location: " . "/review_page.php?id=".$id);
            die();
        }
    }  

    $movie = MovieDao::getById($review->movie_id);
    if (!$movie) {
        header("Location: /");
        die();
    }
    $genres = GenreDao::getAll();
    $languages = LanguageDao::getAll();

    function getDateTimeString($dt) {
        $date=date_create_from_format("Y-m-d H:i:s+", $dt);
        return date_format($date,"d M Y - H:i:s");
    }
    $body = 'templates/review_update_page.php';
    require_once('templates/page.php');
?>

