<?php
    require_once('include.php');    
    if(!isset($_GET['id'])){
        header("Location: /index.php");
        die();
    }
    
    $list_id = $_GET['id'];
    $list = CustomListDao::getById($list_id);
    
    if(!$list) {
        header("Location: /index.php");
        die();
    }

    if((!$is_logged_in || $user->id != $list['user_id']) && $list['is_public'] != 't') {
        header("Location: /index.php");
        die();
    }
    $is_stared = $is_logged_in? CustomListStarDao::is_stared($list_id, $user->id): false; 
    $account_user = UserDao::getUser($list['user_id']);
    $list_items = CustomListItemDao::getMovieItemsByListId($list_id);
    $is_user_self = $is_logged_in? $account_user->id == $user->id : false; 
    $body = 'templates/custom_list_page.php';
    require_once('templates/page.php');
?>