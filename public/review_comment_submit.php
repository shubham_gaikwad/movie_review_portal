<?php
    require_once('include.php');
    $comment = htmlspecialchars($_POST['comment_text']);
        
    $review_id = $_GET['review_id'];

    if($is_logged_in && !$is_disabled){
        $success=CommentDao::insertComment($user->id, $review_id, $comment);
        if ($success) {
            header("Location: /review_page.php?id=$review_id");
            die();
        }
        else {
            echo "error in submitting.";
        }
    } else {
        echo "Log In to submit review.";
    }
?>