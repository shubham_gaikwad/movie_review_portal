<?php 
require_once('include_path.php');

require_once('model/user.php');
require_once('model/movie.php');

require_once('db/connection.php');
require_once('db/genreDao.php');
require_once('db/languageDao.php');
require_once('db/movieDao.php');
require_once('db/userDao.php');
require_once('db/reviewDao.php');
require_once('db/watchlistDao.php');
require_once('db/commentDao.php');
require_once('db/voteDao.php');
require_once('db/actorDao.php');
require_once('db/directorDao.php');
require_once('db/movieActorDao.php');
require_once('db/movieDirectorDao.php');
require_once('db/stats.php');
require_once('db/followDao.php');
require_once('db/notificationDao.php');
require_once('db/customListDao.php');
require_once('db/customListItemDao.php');
require_once('db/customListStarDao.php');
require_once('db/reportDao.php');
require_once('db/reportReasonDao.php');
require_once('db/disableUserDao.php');

function get(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

session_start();
$is_logged_in = false;
if (isset($_SESSION['user']) && isset($_SESSION['user']->id) && isset($_SESSION['user']->rolename)) {
    $is_logged_in = true;
    $user =  $_SESSION['user'];
    $is_disabled_result = DisableUserDao::getUser($user->id);
    if($is_disabled_result != null) {
        $is_disabled = true;
    } else {
        $is_disabled = false;
    }
}

class Template {  
	public $file;
	function __construct($file) {
		$this->file = $file;
	}
    // arg must be an array
    public function set( $values ) {
		foreach( $values as $key => $value ) {
			$this->$key = $value;
		}
    }
  
    // render's html with keys replaced
    public function renderHTML() {
      	include($this->file);
    }
}

$is_admin = false;
if($is_logged_in && $user->rolename === 'Admin')
    $is_admin = true;

?>