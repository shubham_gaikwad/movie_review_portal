<?php
require_once('include.php');
$redirect = get($_POST['redirect'], get($_GET['redirect'], '/index.php'));
$action = $_POST['submit'];

if($is_logged_in){
    if($action === "+")
        WatchlistDao::addToWatchlist($_SESSION['user']->id,$_POST['movie-id']);
    if($action === "-")
        WatchlistDao::deleteFromWatchlist($_SESSION['user']->id,$_POST['movie-id']);
    
    header("Location: $redirect");
    die();
}else
    header("Location: /index.php")
?>