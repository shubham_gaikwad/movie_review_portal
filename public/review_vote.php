<?php
require_once('include.php');

$review_id = $_GET['redirect'];
$vote = $_GET['vote'];

if(!$is_logged_in){
    header("Location: /login.php?redirect=review_page.php?id=".$review_id);
    die();
}

voteDao::registerVote($user->id,$review_id,$vote);
header("Location: /review_page.php?id=".$review_id);
die();
?>