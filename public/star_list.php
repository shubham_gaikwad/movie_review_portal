<?php 
require_once('include.php');
    if(!$is_logged_in) {
    header("Location: /");
    die();
}

if(!isset($_REQUEST['list_id'])) {    
    // header("Location: /");
    echo "no list id";
    die();
}

$list_id = $_REQUEST['list_id'];
$result = CustomListStarDao::insert($list_id, $user->id);
// print_r($list_id);
// print_r($result);
header("Location: /custom_list.php?id=" . $list_id);
?>