<?php
require_once('include.php');

if (isset($_GET['id'])) {
    $review = ReviewDao::getByID($_GET['id']);
    $movie = MovieDao::getById($review->movie_id);

    if (!$review || !$movie) {
        header("Location: /");
        die();
    }

    if ($review->username == $user->username) {
            $userReview = $review;
    }
    $comments = CommentDao::getAllComments($_GET['id']);
    $isVoted = voteDao::voteIs($user->id,$review->id);

} else {
    header("Location: /");
    die();
}

if($is_logged_in && !$is_admin && !$userReview) {
    $report_reasons_review = ReportReasonDao::getAllReasonsByType('REVIEW');
}

$report_reasons_comment = ReportReasonDao::getAllReasonsByType('COMMENT');

$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();

function getDateTimeString($dt) {
    $date=date_create_from_format("Y-m-d H:i:s+", $dt);
    return date_format($date,"d M Y - H:i:s");
}
$body = 'templates/review_page_page.php';
require_once('templates/page.php');
?>