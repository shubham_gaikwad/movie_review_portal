<?php
require_once('include.php');
if (isset($_GET['id'])) {
    $actor = ActorDao::getById($_GET['id']);
    if(!$actor) {
        header("Location: /");
        die();    
    }
    $movies = MovieDao::getByActor($_GET['id']);
} else {
    header("Location: /");
    die();
}

$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$costars = Stats::getCoStarsOfActorWithCount($_GET['id']);
$directors = Stats::getDirectorsOfActorWithCount($_GET['id']);
$genresWorkedIn = Stats::getGenresOfActorWithCount($_GET['id']);
$body = 'templates/actor_page.php';
require_once('templates/page.php');
?>