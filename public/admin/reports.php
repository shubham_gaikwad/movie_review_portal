<?php
require_once('../include.php');
if (!$is_admin) {
    header("Location: /index.php");
    die();
}

$unseen=ReportDao::getAllByType('UNSEEN');
$ignored=ReportDao::getAllByType('IGNORED');
$completed=ReportDao::getAllByType('COMPLETED');


function getDateTimeString($dt)
{
    $date = date_create_from_format("Y-m-d H:i:s+", $dt);
    return date_format($date, "H:i - d M Y");
}

$body = 'templates/admin_reports_page.php';
require_once('templates/page.php');
?>
