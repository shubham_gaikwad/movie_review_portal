<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = ActorDao::insert(htmlspecialchars($_POST['actor']));
    if($success) {
        header('Location: /admin/index.php?ins_actor=1');
        die();
    }
    header('Location: /admin/index.php?err_ins_actor=1');
}
?>