<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = GenreDao::insert(htmlspecialchars($_POST['genre']));
    if($success) {
        header('Location: /admin/index.php?ins_genre=1');
        die();
    }
    header('Location: /admin/index.php?err_ins_genre=1');
}
?>