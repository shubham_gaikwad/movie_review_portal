<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = LanguageDao::insert(htmlspecialchars($_POST['language']));
    if($success) {
        header('Location: /admin/index.php?ins_lang=1');
        die();
    }
    header('Location: /admin/index.php?err_ins_lang=1');
}
?>