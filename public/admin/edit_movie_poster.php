<?php
require_once('../include.php');
if (!$is_admin) {
    header("Location: /index.php");
    die();
}
if(!isset($_REQUEST['id'])) {
    header("Location: /index.php");
    die();
}
$id = $_REQUEST['id'];
$movie = MovieDao::getById($id);
if (!$movie) {
    header("Location: /index.php");
    die();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if($_POST['source'] == 'FILE') {
        $result = handle_file();
        if($result['success']) {
            MovieDao::setMoviePosterByFILE($movie->id, $result['filename']);
            header("Location: /movie.php?id=$id");
        } else {
            $errors = $result['errors'];
        }
    } else {
        MovieDao::setMoviePosterByIMDB($movie->id, $_POST['imdbId']);
        header("Location: /movie.php?id=$id");
    }
}

function handle_file() {
    global $movie;
    $imageFileType = strtolower(pathinfo(basename($_FILES["posterFile"]["name"]),PATHINFO_EXTENSION));
    $target_dir = __DIR__ . "/../movie_poster/";
    $target_file_name = 'poster_' . $movie->id . '.' . $imageFileType;
    $target_file = $target_dir . $target_file_name;
    $check = getimagesize($_FILES["posterFile"]["tmp_name"]);
    $uploadOk = 1;
    $errors = [];
    
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $errors[] = "File is not an image.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["posterFile"]["size"] > 5000000) {
        $errors[] = "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $errors[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $errors[] =  "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["posterFile"]["tmp_name"], $target_file)) {
            //success
        } else {
            $errors[] = "Sorry, there was an error uploading your file.";
        }
    }
    return ['success' => $uploadOk, 'errors' => $errors, 'filename' => "/movie_poster/$target_file_name"];
}

$body = 'templates/admin_edit_movie_poster.php';
require_once('templates/page.php');
?>