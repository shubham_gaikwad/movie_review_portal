<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = DirectorDao::insert(htmlspecialchars($_POST['director']));
    if($success) {
        header('Location: /admin/index.php?ins_director=1');
        die();
    }
    header('Location: /admin/index.php?err_ins_director=1');
}
?>