<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = MovieDao::insert(
        htmlspecialchars($_POST['movie_name']), 
        htmlspecialchars($_POST['release_year']),
        htmlspecialchars($_POST['summary']),
        $_POST['languages'],
        $_POST['genres']);
    if($success) {
        header('Location: /movie.php?id='.$success[0]);
        die();
    }
    header('Location: /admin/index.php?err_ins_movie=1');
}

?>