<?php
require_once('../include.php');
if (!$is_admin) {
    header("Location: /index.php");
    die();
}
if(!isset($_REQUEST['id'])) {
    header("Location: /index.php");
    die();
}

$movie = MovieDao::getById( $_REQUEST['id']);

if(!isset($movie)) {
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = MovieDao::update(
        $_REQUEST['id'],
        htmlspecialchars($_POST['movie_name']), 
        htmlspecialchars($_POST['release_year']),
        htmlspecialchars($_POST['summary']),
        $_POST['languages'],
        $_POST['genres']);
    if($success) {
        header('Location: /movie.php?id='.$_REQUEST['id']);
        die();
    }
}

$m_genres = GenreDao::getByMovie($_GET['id']);
$m_languages = LanguageDao::getByMovie($_GET['id']);
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$body = 'templates/admin_update_movie_page.php';
require_once('templates/page.php');
?>