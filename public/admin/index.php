<?php
require_once('../include.php');
if (!$is_admin) {
    header("Location: /index.php");
    die();
}
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$body = 'templates/admin_index_page.php';
require_once('templates/page.php');
?>
