<?php
require_once('../include.php');
if (!$is_admin) {
    header("Location: /index.php");
    die();
}
if(!isset($_REQUEST['id'])) {
    header("Location: /index.php");
    die();
}
$id = $_REQUEST['id'];
$movie = MovieDao::getById($id);
if (!$movie) {
    header("Location: /index.php");
    die();
}
$m_actors = ActorDao::getByMovie($id);
$body = 'templates/admin_edit_cast.php';
require_once('templates/page.php');
?>