<?php 
require_once('../include.php');
if(!$is_admin) {
    header("Location: /index.php");
    die();
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $report_id=$_REQUEST['report_id'];
    if(!$report_id) {
        header("Location: /index.php");
        die();
    }
    ReportDao::ignore($report_id);
    header('Location: /admin/reports.php');
}
?>