<?php 
require_once('../include.php');
if(!$is_admin) {
    // print_r("not admin");
    header("Location: /index.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_REQUEST['id'];
    $disable = $_POST['disable'];
    if(!$id || !$disable) {
        // print_r($id);
        // print_r($disable);
        // header("Location: /index.php");
        die();
    }
    DisableUserDao::disable($id, $disable);
    header('Location: /account.php?id='.$id);
}
?>