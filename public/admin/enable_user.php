<?php 
require_once('../include.php');
if(!$is_admin) {
    // print_r("not admin");
    header("Location: /index.php");
    die();
}
$id = $_REQUEST['id'];
$redirect = $_REQUEST['redirect'];
if(!$id) {
    // print_r($id);
    // print_r($disable);
    header("Location: /index.php");
    die();
}
DisableUserDao::enable($id);
if($redirect) {
    header('Location: '.$redirect);
    die();
}
header('Location: /account.php?id='.$id);
?>