<?php
    require_once('../include.php');
    if($is_admin){ 
        $success= MovieDao::deleteById($_GET['id']);   
        if ($success) {
            header("Location: /admin/index.php");
            die();
        } else {
            echo "error in deletion.";
        }
    } else {
        echo "Log In to delete review.";
    }
?>