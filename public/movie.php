<?php
require_once('include.php');
if (isset($_GET['id'])) {
    $movie = MovieDao::getById($_GET['id']);

    if (!$movie) {
        header("Location: /");
        die();
    }
    $sortReviewBy = get($_GET['sort'], "popular");

    if($sortReviewBy !== "popular" && $sortReviewBy !== "recent") {
        $sortReviewBy = "popular";
    }

    $m_genres = GenreDao::getByMovie($_GET['id']);
    $m_languages = LanguageDao::getByMovie($_GET['id']);
    $m_reviews = ReviewDao::getAllReviews($_GET['id'], $sortReviewBy);
    $m_actors = ActorDao::getByMovie($_GET['id']);
    $m_directors = DirectorDao::getByMovie($_GET['id']);
    foreach ($m_reviews as $review) {
        if ($review->username == $user->username) {
            $userReview = $review;
            break;
        }
    }
} else {
    header("Location: /");
    die();
}
if($is_logged_in) {
    $custom_lists = CustomListDao::getByUserId($user->id, false);
    $movie_user_custom_list = CustomListItemDao::getListsByUserHavingMovie($user->id, $_GET['id']);
    $report_reasons = ReportReasonDao::getAllReasonsByType('MOVIE');
}
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();

function getReviewString($text) {
    $maxLen = 300;
    if($text == "") return "<i>No review</i>";
    if(strlen($text) <= $maxLen) return $text;
    return substr($text, 0, $maxLen) . "....";
}

function getDateTimeString($dt) {
    $date=date_create_from_format("Y-m-d H:i:s+", $dt);
    return date_format($date,"d M Y - H:i:s");
}

$body = 'templates/movie_page.php';
require_once('templates/page.php');

?>

<?php function my_array_search($val, $index_arr){
    foreach ($index_arr as $key => $value) {
        if($val == $value) {
            return true;
        }
    }
    return false;
} ?>