<?php
require_once('../include.php');
if (isset($_GET['id1']) && isset($_GET['id2'])) {
    $actor1 = ActorDao::getById($_GET['id1']);
    if(!$actor1) {
        header("Location: /");
        die();    
    }
    $actor2 = ActorDao::getById($_GET['id2']);
    if(!$actor2) {
        header("Location: /");
        die();    
    }
} else {
    header("Location: /");
    die();
}
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$movies = Stats::getMoviesRefActorActor($actor1['id'], $actor2['id']);
$body = 'templates/ref_actor_actor.php';
require_once('templates/page.php');
?>