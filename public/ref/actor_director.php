<?php
require_once('../include.php');
if (isset($_GET['a']) && isset($_GET['d'])) {
    $actor = ActorDao::getById($_GET['a']);
    if(!$actor) {
        header("Location: /");
        die();    
    }
    $director = DirectorDao::getById($_GET['d']);
    if(!$director) {
        header("Location: /");
        die();    
    }
} else {
    header("Location: /");
    die();
}
$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$movies = Stats::getMoviesRefActorDirector($actor['id'], $director['id']);
$body = 'templates/ref_actor_director.php';
require_once('templates/page.php');
?>