<?php
require_once('../include.php');
if (isset($_GET['d']) && isset($_GET['g'])) {
    $director = DirectorDao::getById($_GET['d']);
    if(!$director) {
        header("Location: /");
        die();    
    }
} else {
    header("Location: /");
    die();
}
$genres = GenreDao::getAll();
if(!isset($genres[$_GET['g']])) {
    header("Location: /");
    die();
}
$genre = $genres[$_GET['g']];
$languages = LanguageDao::getAll();
$movies = Stats::getMoviesRefDirectorGenre($director['id'], $_GET['g']);
$body = 'templates/ref_director_genre.php';
require_once('templates/page.php');
?>