<?php
require_once('../include.php');
if (isset($_GET['a']) && isset($_GET['g'])) {
    $actor = ActorDao::getById($_GET['a']);
    if(!$actor) {
        header("Location: /");
        die();    
    }
} else {
    header("Location: /");
    die();
}
$genres = GenreDao::getAll();
if(!isset($genres[$_GET['g']])) {
    header("Location: /");
    die();
}
$genre = $genres[$_GET['g']];
$languages = LanguageDao::getAll();
$movies = Stats::getMoviesRefActorGenre($actor['id'], $_GET['g']);
$body = 'templates/ref_actor_genre.php';
require_once('templates/page.php');
?>