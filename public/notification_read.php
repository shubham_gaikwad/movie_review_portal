<?php
require_once('include.php');
$id=$_GET['id'];
if(!$id || !$is_logged_in) {
    header("Location: /notifications.php");
    die();
}
$row = NotificationDao::getNotificationById($id);
if(!$row || $row->recipient_id != $user->id) {
    header("Location: /notifications.php");
    die();
}
NotificationDao::readNotification($id);
if($row->uri) {
    header("Location: ".$row->uri);
} else {
    header("Location: /notifications.php");
}
?>