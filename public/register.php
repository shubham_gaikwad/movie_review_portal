<?php
require_once('include.php');

$redirect = get($_POST['redirect'], get($_GET['redirect'], '/index.php'));
$action = $_SERVER['SCRIPT_NAME'] . "?redirect=$redirect";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $user = new User();
    $user->username = htmlspecialchars($_REQUEST['username']);
    $user->email = htmlspecialchars($_REQUEST['email']);
    $user->rolename = 'General';
    $user->profile_photo_name = 'default.png';
    $pass = $_REQUEST['pass'];
    $hash = password_hash($pass, PASSWORD_ARGON2I);
    $success = UserDao::createUser($user, $hash);

    if ($success) {
        $_SESSION['user'] = UserDao::authenticateUser($user->username, $pass);
        header("Location: $redirect");
        die();
    } else {
        $registrationFailed = true;
    }
}

if ($is_logged_in) {
    header("Location: $redirect");
    die();
}

require_once('templates/register_page.php');
?>