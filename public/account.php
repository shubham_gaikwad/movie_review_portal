<?php
require_once('include.php');

$account_user_id = get($_GET['id'], $is_logged_in? $user->id: null);

if(!$account_user_id)
    header("Location: /");
$account_user = UserDao::getUser($account_user_id);

if(!$account_user)
    header("Location: /");

$disabled_user = DisableUserDao::getUser($account_user_id);
$is_disabled = !!$disabled_user;

$is_user_self = $is_logged_in? $account_user_id == $user->id : false; 
$reviews = ReviewDao::getALLReviewsByUser($account_user_id);

if($is_user_self) {
    $watchlist = WatchlistDao::getWatchlist($account_user_id);
    $followings = FollowDao::getFollowings($account_user_id);
    $followers = FollowDao::getFollowers($account_user_id);
    $starred_lists = CustomListDao::getStarredByUserId($account_user_id);
}

$custom_lists = CustomListDao::getByUserId($account_user_id, !$is_user_self);

$show_follow_unfollow = !$is_user_self && $is_logged_in;
if($show_follow_unfollow)
    $isFollowing = FollowDao::is_following($user->id, $account_user_id);
    
$following_count = FollowDao::countFollowing($account_user_id);
$follower_count = FollowDao::countFollowers($account_user_id);

$report_reasons = ReportReasonDao::getAllReasonsByType('USER');

function getReviewString($text) {
    $maxLen = 300;
    if($text == "") return "<i>No review</i>";
    if(strlen($text) <= $maxLen) return $text;
    return substr($text, 0, $maxLen) . "....";
}

function getDateTimeString($dt) {
    $date=date_create_from_format("Y-m-d H:i:s+", $dt);
    return date_format($date,"d M Y - H:i:s");
}

$body = 'templates/account_page.php';
require_once('templates/page.php');
?>