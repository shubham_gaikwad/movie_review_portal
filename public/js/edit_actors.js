function remove_actor(id) {
    let elm = document.querySelector(`[data-actor-id="${id}"]`);
    if (!elm) {
        console.error("actor not found with id", id);
        return;
    }
    elm.remove();
}
const debounce = (func, delay) => {
    let debounceTimer
    return function () {
        const context = this
        const args = arguments
        clearTimeout(debounceTimer)
        debounceTimer
            = setTimeout(() => func.apply(context, args), delay)
    }
}

function suggestionListItem(aid, aname) {
    return `<li class="list-group-item list-group-item-info" 
        data-search-actor-id="${aid}" 
        data-search-actor-name="${aname}">
        <span class="h5">
            ${aname}
        </span>
        <button style="float:right; border:none; background-color:inherit;"  onclick="add_actor(${aid})">
            <span class="material-icons">
                add
            </span>
        </button>
    </li>`;
}

function suggestionList(json) {
    let html = "";
    for (const key in json) {
        if (Object.hasOwnProperty.call(json, key)) {
            const element = json[key];
            html += suggestionListItem(key, element);
        }
    }
    return html;
}
const fetchActorsClbk = (e) => {
    console.log(e.target.value);
    if (e.target.value == '')
        return;
    fetch(`/ajax/search_actor.php?q=${encodeURIComponent(e.target.value)}`)
        .then((res) => res.json())
        .then((json) => {
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(suggestionList(json));
            // console.log(html)
        }).catch(e => console.error(e));
}

document.getElementById("search-box").addEventListener("keyup", debounce(fetchActorsClbk, 1000));

document.getElementById("search-box").addEventListener("focusin", fetchActorsClbk);

$("#clear-search").click(function () {
    $("#suggesstion-box").hide();
});

function add_actor(id) {
    console.log("add actor", id)
    if (document.querySelector(`#selections > [data-actor-id="${id}"]`))
        return;
    let elm = document.querySelector(`[data-search-actor-id="${id}"]`);
    //console.log(elm.dataset, elm)
    let selections = document.getElementById("selections");
    //<li data-actor-id=<?=$key?>><?=$value?><button onclick="remove_actor(<?=$key?>)">Remove</button></li>
    let li = document.createElement("li");
    li.classList.add("list-group-item");
    li.setAttribute("data-actor-id", elm.dataset.searchActorId);
    li.innerHTML = `
    <span class="h5">${elm.dataset.searchActorName}</span>                    
    <button style="float:right;" 
        class="btn btn-danger btn-sm d-flex justify-content-center align-content-between" 
        onclick="remove_actor(${elm.dataset.searchActorId})">
        <span class="material-icons">
            delete    
        </span>
    </button>`;
    selections.appendChild(li);
    $("#suggesstion-box").hide();
}

function save() {
    let selections = document.querySelectorAll(`#selections > [data-actor-id]`);
    let arr = [];
    selections.forEach(element => {
        arr.push(element.dataset.actorId);
    });
    let url = new URL(window.location.href);
    let params = new URLSearchParams(url.search);

    fetch(`/ajax/save_cast.php?id=${params.get("id")}`, { method: "POST", body: JSON.stringify(arr) })
        .then(res => { window.location.assign("/movie.php?id=" + params.get("id")) });
}
