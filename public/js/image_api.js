const image_base_url = "https://image.tmdb.org/t/p/w500";
const default_poster_path = "/images/default_poster.svg";
const default_actor_path = "/images/default_profile_2_by_3.png"

const find_movie = (id) => {
    return fetch(
        `https://api.themoviedb.org/3/find/${id}?api_key=cd82983e0494475150baf33f38acdda2&language=en-US&external_source=imdb_id`
    ).then(resp => resp.json());
}

const find_poster_path = (id) => {
    return find_movie(id)
        .then(json => json.movie_results[0].poster_path)
        .then(path => path == null ? default_poster_path : image_base_url + path)
        .catch(err => default_poster_path)
}

const find_person = (name) => {
    return fetch(
        `https://api.themoviedb.org/3/search/person?api_key=cd82983e0494475150baf33f38acdda2&language=en-US&query=${encodeURIComponent(name)}&page=1&include_adult=false`
    ).then(resp => resp.json());
}

const find_actor_profile_path = (name) => {
    return find_person(name)
        .then(json => json.results.find(a => a.name == name && a.known_for_department == "Acting").profile_path)
        .then(path => path == null ? default_actor_path : image_base_url + path)
        .catch(err => default_actor_path)
}

const find_director_profile_path = (name) => {
    return find_person(name)
        .then(json => json.results.find(a => a.name == name && a.known_for_department == "Directing").profile_path)
        .then(path => path == null ? default_actor_path : image_base_url + path)
        .catch(err => default_actor_path)
}

window.onload = () => {
    const image_nodes = document.querySelectorAll("img[data-imdb-id]");
    image_nodes.forEach(async (node) => {
        node.src = await find_poster_path(node.dataset.imdbId);
    });

    const actor_nodes = document.querySelectorAll("img[data-actor]");
    actor_nodes.forEach(async (node) => {
        node.src = await find_actor_profile_path(node.dataset.actor);
    });

    const director_nodes = document.querySelectorAll("img[data-director]");
    director_nodes.forEach(async (node) => {
        node.src = await find_director_profile_path(node.dataset.director);
    });
};