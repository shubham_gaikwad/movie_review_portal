function remove_director(id) {
    let elm = document.querySelector(`[data-director-id="${id}"]`);
    if (!elm) {
        console.error("director not found with id", id);
        return;
    }
    elm.remove();
}
const debounce = (func, delay) => {
    let debounceTimer
    return function () {
        const context = this
        const args = arguments
        clearTimeout(debounceTimer)
        debounceTimer
            = setTimeout(() => func.apply(context, args), delay)
    }
}

function suggestionListItem(aid, aname) {
    return `<li class="list-group-item list-group-item-info" 
        data-search-director-id="${aid}" 
        data-search-director-name="${aname}">
        <span class="h5">
        ${aname}
        </span>
        <button style="float:right; border:none; background-color:inherit;"  onclick="add_director(${aid})">
            <span class="material-icons">
                add
            </span>
        </button>
    </li>`;
}

function suggestionList(json) {
    let html = "";
    for (const key in json) {
        if (Object.hasOwnProperty.call(json, key)) {
            const element = json[key];
            html += suggestionListItem(key, element);
        }
    }
    return html;
}
const fetchDirectorClbk = (e) => {
    console.log(e.target.value);
    if (e.target.value == '')
        return;
    fetch(`/ajax/search_director.php?q=${encodeURIComponent(e.target.value)}`)
        .then((res) => res.json())
        .then((json) => {
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(suggestionList(json));
            // console.log(html)
        }).catch(e => console.error(e));
}
document.getElementById("search-box").addEventListener("keyup", debounce(fetchDirectorClbk, 1000));

document.getElementById("search-box").addEventListener("focusin", fetchDirectorClbk);

$("#clear-search").click(function () {
    $("#suggesstion-box").hide();
});

function add_director(id) {
    if (document.querySelector(`#selections > [data-director-id="${id}"]`))
        return;
    let elm = document.querySelector(`[data-search-director-id="${id}"]`);
    //console.log(elm.dataset, elm)
    let selections = document.getElementById("selections");
    //<li data-director-id=<?=$key?>><?=$value?><button onclick="remove_director(<?=$key?>)">Remove</button></li>
    let li = document.createElement("li");
    li.setAttribute("data-director-id", elm.dataset.searchDirectorId);
    li.classList.add("list-group-item");
    li.innerHTML = `
    <span class="h5">${elm.dataset.searchDirectorName}</span>                    
    <button style="float:right;" 
        class="btn btn-danger btn-sm d-flex justify-content-center align-content-between" 
        onclick="remove_director(${elm.dataset.searchDirectorId})">
        <span class="material-icons">
            delete    
        </span>
    </button>`;
    selections.appendChild(li);
    $("#suggesstion-box").hide();
}

function save() {
    let selections = document.querySelectorAll(`#selections > [data-director-id]`);
    let arr = [];
    selections.forEach(element => {
        arr.push(element.dataset.directorId);
    });
    let url = new URL(window.location.href);
    let params = new URLSearchParams(url.search);

    fetch(`/ajax/save_director.php?id=${params.get("id")}`, { method: "POST", body: JSON.stringify(arr) })
        .then(res => { window.location.assign("/movie.php?id=" + params.get("id")) });
}
