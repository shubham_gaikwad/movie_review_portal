(() => {

    const debounce = (func, delay) => {
        let debounceTimer
        return function () {
            const context = this
            const args = arguments
            clearTimeout(debounceTimer)
            debounceTimer
                = setTimeout(() => func.apply(context, args), delay)
        }
    }

    const types = {
        "TYPE_ACTOR": {
            getURL: (id) => `/actor.php?id=${id}`,
            getType: "[Actor]"
        },

        "TYPE_DIRECTOR": {
            getURL: (id) => `/director.php?id=${id}`,
            getType: "[Director]"
        },

        "TYPE_MOVIE": {
            getURL: (id) => `/movie.php?id=${id}`,
            getType: "[Movie]"
        },

        "TYPE_USER": {
            getURL: (id) => `/account.php?id=${id}`,
            getType: "[User]"
        }
    }

    function suggestionListItem(id, title, type) {
        return `<li class="list-group-item">
        <a class="a" href="${types[type].getURL(id)}">
            ${types[type].getType} ${title}
        </a>
    </li>`;
    }

    function suggestionList(json) {
        let html = "";
        for (const obj of json) {
            html += suggestionListItem(obj.id, obj.title, obj.type);
        }
        return html;
    }

    const fetchClbk = (e) => {
        console.log(e.target.value);
        if (e.target.value == '') {
            $("#nav-suggesstion-box").hide();
            return;
        }
        fetch(`/ajax/search_actor_director_movie.php?q=${encodeURIComponent(e.target.value)}`)
            .then((res) => res.json())
            .then((json) => {
                $("#nav-suggesstion-box").show();
                $("#nav-suggesstion-box").html(suggestionList(json));
                // console.log(html)
            }).catch(e => console.error(e));
    }

    document.getElementById("nav-search-input").addEventListener("keyup", debounce(fetchClbk, 50));

    document.getElementById("nav-search-input").addEventListener("focusin", debounce(fetchClbk, 50));

    document.getElementById("nav-search-form").addEventListener("focusout", () => {
        setTimeout(() => $("#nav-suggesstion-box").hide(), 500);
    });

    $('#nav-search-input').on('search', function () {
        $("#nav-suggesstion-box").hide();
    });
})()