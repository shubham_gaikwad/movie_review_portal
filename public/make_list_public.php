<?php 
require_once('include.php');
    if(!$is_logged_in) {
    header("Location: /");
    die();
}

if(!isset($_POST['list_id'])) {    
    header("Location: /");
    die();
}

$list_id = $_POST['list_id'];
$list = CustomListDao::getById($list_id);
if(!$list || $list['user_id'] != $user->id) {
    header("Location: /");
    die();
}
$result = CustomListDao::make_public($list_id);
if($result) {
    header("Location: /custom_list.php?id=$list_id");
    die();
}
header("Location: /");
?>