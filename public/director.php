<?php
require_once('include.php');
if (isset($_GET['id'])) {
    $director = DirectorDao::getById($_GET['id']);
    if(!$director) {
        header("Location: /");
        die();    
    }
    $movies = MovieDao::getByDirector($_GET['id']);
} else {
    header("Location: /");
    die();
}

$genres = GenreDao::getAll();
$languages = LanguageDao::getAll();
$coActors = Stats::getActorsOfDirectorWithCount($_GET['id']);
$genresWorkedIn = Stats::getGenresOfDirectorWithCount($_GET['id']);
$body = 'templates/director_page.php';
require_once('templates/page.php');
?>