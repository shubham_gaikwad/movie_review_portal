<?php
    require_once('include.php');    
    if(!$is_logged_in){
        header("Location: /index.php");
        die();
    }
    $redirect = get($_REQUEST['redirect'], '/account.php#custom-list');
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        CustomListDao::insert($user->id, $_POST['list_name'], isset($_POST['private']));
        header("Location: $redirect");
        die();    
    }
    $body = 'templates/create_custom_list_page.php';
    require_once('templates/page.php');
?>