<?php
require_once('../include.php');
if (!$is_admin) {
    echo "{\"success\":false}";
    die();
}
if(!isset($_REQUEST['id']) || !isset($_REQUEST['reason'])) {
    echo "{\"success\":false}";
    die();
}
$id = $_REQUEST['id'];
$reason = $_REQUEST['reason'];
$success = NotificationDao::warnUser($id, $reason);
if($success)
    echo "{\"success\":true}";
else 
    echo "{\"success\":false}"
?>