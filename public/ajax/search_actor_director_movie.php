<?php
    require_once('../include.php');
    if(!isset($_REQUEST['q'])) {
        echo "";
        die();
    }
    $name = urldecode($_REQUEST['q']);
    $actors = ActorDao::searchByName($name);
    $directors = DirectorDao::searchByName($name);
    $movies = MovieDao::searchByName($name);
    $users = UserDao::searchByName($name);

    $result = [];

    foreach($actors as $key => $value) {
        $result[] = ["id" => $key, "title" => $value, "type" => "TYPE_ACTOR"];
    }

    foreach($directors as $key => $value) {
        $result[] = ["id" => $key, "title" => $value, "type" => "TYPE_DIRECTOR"];
    }

    foreach($movies as $key => $value) {
        $result[] = ["id" => $key, "title" => $value, "type" => "TYPE_MOVIE"];
    }

    foreach($users as $key => $value) {
        $result[] = ["id" => $key, "title" => $value, "type" => "TYPE_USER"];
    }
    
    echo json_encode($result);
?>