<?php
    require_once('../include.php');
    if(!isset($_REQUEST['q'])) {
        echo "";
        die();
    }
    $name = urldecode($_REQUEST['q']);
    $actors = ActorDao::searchByName($name);
    echo json_encode($actors);
?>
       
