<?php 
require_once('../include.php');
if(!$is_logged_in) {
    echo json_encode(["success" => false]);
    die();
}

if(!isset($_REQUEST['movie_id'])) {
    echo json_encode(["success" => false]);
    die();
}
$movie_id = $_REQUEST['movie_id'];
$movie = MovieDao::getById($movie_id);
if (!$movie) {
    echo json_encode(["success" => false]);
    die();
}
if(!isset($_REQUEST['list_id'])) {
    echo json_encode(["success" => false]);
    die();
}
$list_id = $_REQUEST['list_id'];
$list = CustomListDao::getById($list_id);
if(!$list || $list['user_id'] != $user->id) {
    echo json_encode(["success" => false]);
    die();
}
$result = CustomListItemDao::insert($list_id, $movie_id);
if($result) {
    echo json_encode(["success" => true]);
    die();
}
echo json_encode(["success" => false]);
?>