<?php
require_once('../include.php');
if (!$is_logged_in) {
    echo "{\"success\":false}";
    die();
}
if(!isset($_REQUEST['reason_id']) || !isset($_REQUEST['resource_id'])) {
    echo "{\"success\":false}";
    die();
}
$reason_id = $_REQUEST['reason_id'];
$resource_id = $_REQUEST['resource_id'];
$success = ReportDao::insert($user->id, $resource_id, 'USER', $reason_id);
if($success)
    echo "{\"success\":true}";
else 
    echo "{\"success\":false}"
?>