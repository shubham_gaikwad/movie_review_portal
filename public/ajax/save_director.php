<?php
require_once('../include.php');
if (!$is_admin) {
    echo "{'success':false}";
    die();
}
if(!isset($_REQUEST['id'])) {
    echo "{'success':false}";
    die();
}
$id = $_REQUEST['id'];
$movie = MovieDao::getById($id);
if (!$movie) {
    echo "{'success':false}";
    die();
}
$entityBody = file_get_contents('php://input');
$body = json_decode($entityBody);
$success = MovieDirectorDao::save_director($id, $body);
if($success)
    echo "{'success':true}";
else 
    echo "{'success':false}"
?>