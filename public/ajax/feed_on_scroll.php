<?php

require_once('../include.php');
if(!$is_logged_in) {
    echo "{'success': false}";
    exit;
}

$last_posted_at = $_REQUEST['last_posted_at'];

if(!$last_posted_at) {
    echo "{'success': false}";
    exit;
}

// $last_posted_at = '2021-04-09 23:45:35.511393';

$feed_next = ReviewDao::getFeedNext($user->id, $last_posted_at);
$result = ["success" => true, "data" => $feed_next];
$str = json_encode($result);
echo $str;
?>