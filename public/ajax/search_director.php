<?php
    require_once('../include.php');
    if(!isset($_REQUEST['q'])) {
        echo "";
        die();
    }
    $name = urldecode($_REQUEST['q']);
    $directors = DirectorDao::searchByName($name);
    echo json_encode($directors);
?>
