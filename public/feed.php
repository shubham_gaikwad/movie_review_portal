<?php
    require_once('include.php');
    function getReviewString($text) {
        $maxLen = 300;
        if($text == "") return "<i>No review</i>";
        if(strlen($text) <= $maxLen) return $text;
        return substr($text, 0, $maxLen) . "....";
    }
    
    function getDateTimeString($dt) {
        $date=date_create_from_format("Y-m-d H:i:s+", $dt);
        return date_format($date,"d M Y - H:i:s");
    }

    $feed = ReviewDao::getFeed($user->id);
    $body = 'templates/feed_page.php';
    require_once('templates/page.php');
?>