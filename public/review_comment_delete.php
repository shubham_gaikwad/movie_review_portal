<?php
    require_once('include.php');
    $comment = CommentDao::getByID($_GET['comment_id']);
    if (isset($comment)){
        if($is_logged_in && ($comment->user_id === $user->id || $is_admin)){ 
            $success=CommentDao::deleteByID($comment->id);
        
            if ($success) {
                header("Location: /review_page.php?id=$comment->review_id");
                die();
            } else {
                echo "error in deletion.";
            }
        } else {
            echo "Log In to delete comment.";
        }
    } else {
        echo "Comment doesn't exist.";
    } 
?>