<?php
    require_once('include.php');
    $movies = MovieDao::test();
    $genres = GenreDao::getAll();
    $languages = LanguageDao::getAll();
    $body = 'templates/index_page.php';
    require_once('templates/page.php');
?>