<?php
    require_once('include.php');
    $review = ReviewDao::getByID($_GET['review_id']);

    if (isset($review)){
        if($is_logged_in && ($review->user_id === $user->id || $is_admin)){ 
            $success=ReviewDao::deleteByID($review->id);
        
            if ($success) {
                header("Location: /movie.php?id=$review->movie_id");
                die();
            } else {
                echo "error in deletion.";
            }
        } else {
            echo "Log In to delete review.";
        }
    } else {
        echo "Review doesn't exist.";
    }  
?>