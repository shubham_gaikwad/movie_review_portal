<div class="container my-5">

    <div class="jumbotron jumbotron-fluid bg-white">
        <div class="container">
            <h2>
                <?php if($is_user_self): ?>
                Your custom list
                <?php else : ?>
                <a href="/account.php?id=<?= $account_user->id?>"><?= $account_user->username?></a>'s
                custom
                list
                <?php endif; ?>
                (<?= $list["movie_count"] ?>)
            </h2>
            <hr class="my-4">
            <p class="lead">
            <div class="d-flex flex-row">
                <span class="flex-grow-1 d-flex flex-row align-items-start">
                    <?php if($is_user_self) : ?>
                    <?php if($list['is_public'] == 't') :?>
                    <span class="badge badge-warning">Public
                    </span>
                    <?php else :?>
                    <span class="badge badge-info">Private
                    </span>
                    <?php endif;?>
                    <?php endif;?>
                    <span class="ml-2 h3"><?= $list['name'] ?></span>
                </span>

                <?php if($is_logged_in) : ?>
                <div class="btn-group btn-group-toggle d-flex flex-row align-items-center" data-toggle="buttons">
                    <form
                        action="<?= ($is_stared == true? "unstar_list.php": "star_list.php") . "?list_id=" . $list["id"] ?>"
                        method="post">
                        <button type="submit" class="btn btn-outline-dark d-flex flex-row align-items-center">
                            <?php if(!$is_stared) :?>
                            <span class="material-icons flex-grow-1">
                                star_border
                            </span>
                            <span class="ml-1">Star</span>
                            <?php else: ?>
                            <span class="material-icons flex-grow-1">
                                star
                            </span>
                            <span class="ml-1">Unstar</span>
                            <?php endif; ?>
                        </button>
                    </form>
                    <label class="btn btn-outline-dark">
                        <?=$list['star_count']?>
                    </label>
                </div>
                <?php endif; ?>

                <?php if($is_user_self) : ?>
                <span class="d-flex flex-row flex-grow-1 align-items-center justify-content-end">
                    <?php if($list['is_public'] == 't') :?>
                    <button type="button" data-toggle="modal" data-target="#private-list-modal"
                        class="btn btn-outline-info d-flex flex-row align-items-center">

                        Make Private
                        <span class="material-icons ml-2">
                            lock
                        </span>
                    </button>
                    <?php else :?>
                    <button type="button" data-toggle="modal" data-target="#public-list-modal"
                        class="btn btn-outline-warning d-flex flex-row align-items-center">

                        Make Public
                        <span class="material-icons ml-2">
                            lock_open
                        </span>
                    </button>
                    <?php endif;?>
                    <button type="button" data-toggle="modal" data-target="#delete-list-modal"
                        class="ml-2 btn btn-outline-danger d-flex flex-row align-items-center">
                        Delete List
                        <span class="material-icons ml-2">
                            delete_forever
                        </span>
                    </button>
                </span>
                <?php endif; ?>
            </div>
            </p>
            <hr class="my-4">
        </div>
    </div>

    <ul class="list-group">
        <?php foreach ($list_items as $movie) : ?>
        <li class="list-group-item border-0" id="movie-row-<?= $movie['movie_id'] ?>">
            <a href="/movie.php?id=<?= $movie['movie_id'] ?>">
                <span style="float: left;">
                    <img class="rounded shadow" <?php if($movie['show_poster_from_imdb'] == 't'):?>
                        data-imdb-id=<?= $movie['imdb_id'] ?> <?php endif;?> width="120" height="150"
                        src="<?= $movie['movie_poster_name'] ?>" alt="<?= $movie['movie_name'] ?> Poster">
                </span>
                <span class="ml-3 h3">
                    <?= $movie['movie_name'] ?>
                </span>
            </a>
            <?php if($is_user_self) : ?>
            <button class="btn btn-sm btn-warning d-flex flex-row align-items-center"
                data-movie-id="<?= $movie['movie_id'] ?>" onclick="remove_movie(<?= $movie['movie_id'] ?>)"
                style="float: right;">
                Remove
                <span class="material-icons ml-2">
                    clear
                </span>
            </button>
            <?php endif; ?>
        </li>
        <?php endforeach; ?>
    </ul>

    <div class="modal fade" id="delete-list-modal" tabindex="-1" role="dialog" aria-labelledby="delete-list-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="delete-list-modal-label">Delete Custom List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure? It will permanantly delete the list
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <form action="/delete_custom_list.php" method="post">
                        <input type="number" value="<?=$list['id']?>" name="list_id" hidden>
                        <button type="submit" class="btn btn-danger">Delete
                            Anyway</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="private-list-modal" tabindex="-1" role="dialog"
        aria-labelledby="private-list-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="private-list-modal-label">Make List Private</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure? <br> Other users won't be able to see and all the stars will vanish
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <form action="/make_list_private.php" method="post">
                        <input type="number" value="<?=$list['id']?>" name="list_id" hidden>
                        <button type="submit" class="btn btn-info" onclick="">Make Private</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="public-list-modal" tabindex="-1" role="dialog" aria-labelledby="public-list-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="public-list-modal-label">Make List Public</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure? <br> Anyone can view and star the list
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <form action="/make_list_public.php" method="post">
                        <input type="number" value="<?=$list['id']?>" name="list_id" hidden>
                        <button type="submit" class="btn btn-warning" onclick="">Make Public</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function remove_movie(id) {
    console.log("remove movie", id);
    fetch(`/ajax/remove_from_list.php?movie_id=${id}&list_id=<?=$list['id']?>`)
        .then(res => res.json())
        .then(json => {
            if (json.success) {
                $(`#movie-row-${id}`).remove();
            }
        });
}
</script>