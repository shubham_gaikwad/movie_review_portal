<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>    
    <?php require_once("templates/components/head.html")?>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">WTW</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card my-5" style="width: 400px;">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <form action="<?= $action?>" method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" placeholder="Enter username" id="username" name="username" required>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" placeholder="Enter password" id="pwd" name="pass" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <?php if($loginFailed) :?>
                    <div class="alert alert-danger">
                        <strong>Login Failed!</strong> Incorrect Username or Password.
                    </div>
                <?php endif; ?>
                <div class="card-footer"><a href="/register.php?redirect=<?= $redirect?>">not an existing user?</a></div>
            </div>
        </div>
    </div>
</body>

</html>