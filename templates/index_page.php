<div class="container my-5">
    <?php if ($is_logged_in) : ?>
    <h1 class="my-5">Welcome,
        <a class="text-primary" href="/account.php" style="text-decoration: none;"><?= $user->username ?></a>!
    </h1>
    <hr class="my-1">
    <?php endif; ?>
    <?php 
        $english = new Template('templates/components/recent_movies.php');
        $english->language = "English";
        $english->movies = $movies[0];
        $english->link = "/search.php?search=&genre=&lang=63&sort=4";
        $english->renderHTML();

        $hindi = new Template('templates/components/recent_movies.php');
        $hindi->language = "Hindi";
        $hindi->movies = $movies[1];
        $hindi->link = "/search.php?search=&genre=&lang=93&sort=4";
        $hindi->renderHTML();


        $marathi = new Template('templates/components/recent_movies.php');
        $marathi->language = "Marathi";
        $marathi->movies = $movies[2];
        $marathi->link = "/search.php?search=&genre=&lang=147&sort=4";
        $marathi->renderHTML();
    ?>
</div>