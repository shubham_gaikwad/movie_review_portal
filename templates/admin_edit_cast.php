<div class="container my-5">
    <?php 
        $card = new Template('templates/components/movie_row_item.php');
        $card->movie_id = $movie->id;
        $card->movie_imdb_id = $movie->imdb_id;
        $card->movie_poster_name = $movie->movie_poster_name;
        $card->movie_name = $movie->movie_name;
        $card->movie_show_poster_from_imdb = $movie->show_poster_from_imdb;
        $card->renderHTML();
    ?>
    <div class="card bg-light mb-3 w-75">
        <div class="card-header h3">Edit Cast</div>
        <div class="card-body">
            <div class="frmSearch" style="position:relative;">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="search-box" placeholder="Search Actor" />
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary d-flex justify-content-center align-content-between"
                            id="clear-search" type="button">
                            <span class="material-icons">
                                clear
                            </span>
                        </button>
                    </div>

                </div>
                <ul style="position:absolute; z-index: 10;" class="list-group w-50" id="suggesstion-box"></ul>
            </div>

            <ul class="list-group" id="selections">
                <?php foreach ($m_actors as $key => $value) { ?>
                <li class="list-group-item" data-actor-id=<?=$key?>>
                    <span class="h5"><?=$value?></span>
                    <button style="float:right;"
                        class="btn btn-danger btn-sm d-flex justify-content-center align-content-between"
                        onclick="remove_actor(<?=$key?>)">
                        <span class="material-icons">
                            delete
                        </span>
                    </button>
                </li>
                <?php } ?>
            </ul>
            <button class="mt-4 btn btn-primary btn-lg btn-block" onclick="save()">Save</button>
        </div>
    </div>
</div>
<script src="/js/edit_actors.js"></script>