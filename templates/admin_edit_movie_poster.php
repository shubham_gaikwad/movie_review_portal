<div class="container my-5">
    <?php 
        $card = new Template('templates/components/movie_row_item.php');
        $card->movie_id = $movie->id;
        $card->movie_imdb_id = $movie->imdb_id;
        $card->movie_poster_name = $movie->movie_poster_name;
        $card->movie_name = $movie->movie_name;
        $card->movie_show_poster_from_imdb = $movie->show_poster_from_imdb;
        $card->renderHTML();
    ?>
    <div class="card bg-light mb-3 w-75">
        <div class="card-header h3">Edit Movie Poster</div>
        <div class="card-body">
            <form action="edit_movie_poster.php?id=<?=$id?>" method="POST" enctype="multipart/form-data">
                <h4>Choose Source</h4>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="source" id="radios1" value="TMDB" required>
                    <label class="form-check-label" for="radios1">
                        TMDB Poster API
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="source" id="radios2" value="FILE">
                    <label class="form-check-label" for="radios2">
                        Upload File
                    </label>
                </div>
                <div class="form-group container" id="imdb-id-panel">
                    <label for="imdbIdInput">IMDB ID</label>
                    <input type="text" class="form-control" id="imdbIdInput" placeholder="IMDB ID" name="imdbId"
                        value="<?=$movie->imdb_id?>">

                    <div class="my-2">
                        <button type="button" class="btn btn-info" id="testImageBtn">Test</button>
                    </div>
                    <div><img width="100px" alt="Poster from TMDB" id="testImage"></div>

                    <script>
                    function testImage() {
                        let id = document.getElementById('imdbIdInput').value;
                        let imgNode = document.getElementById('testImage');
                        find_poster_path(id).then(path => imgNode.src = path);
                    }
                    document.getElementById('testImageBtn').addEventListener('click', testImage);
                    </script>
                </div>
                <div class="form-group container" id="file-panel">
                    <label for="fileInput">Poster</label>
                    <input type="file" class="form-control-file" id="fileInput" name="posterFile">
                </div>
                <input class="btn btn-primary" type="submit" value="Save">
            </form>
            <?php if(isset($errors)) :?>
            <div class="alert alert-danger">
                <strong>Failure!</strong> <?= join("<br>", $errors) ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<script>
$('#radios2').prop("checked", true);
$('#file-panel').show();
$('#imdb-id-panel').hide();
$('input[type=radio][name=source]').change(function() {
    if (this.value == 'TMDB') {
        $('#file-panel').hide();
        $('#imdb-id-panel').show();
    } else if (this.value == 'FILE') {
        $('#file-panel').show();
        $('#imdb-id-panel').hide();
    }
});
</script>