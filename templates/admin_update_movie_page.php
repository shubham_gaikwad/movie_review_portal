<div class="container my-5">
    <div class="card" style="width:70%">
        <div class="card-header h4">
            Update Movie
        </div>
        <div class="card-body">
            <form action="update_movie.php?id=<?=$_REQUEST['id'] ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Movie Name</label>
                    <input type="text" name="movie_name" class="form-control" placeholder="Movie Name" value="<?= $movie->movie_name ?>" required>
                </div>
                <div class="form-group">
                    <label>Release Year</label>
                    <input type="number" name="release_year" class="form-control" placeholder="Release Year" value="<?= $movie->release_year ?>" required>
                </div>
                <div class="form-group">
                    <label>Summary</label>
                    <textarea name="summary" cols="30" rows="10" class="form-control" placeholder="Summary"><?= $movie->summary ?></textarea>
                </div>         
                <div class="my-3">
                    <h5>Select Languages</h5>
                    <select name="languages[]" class="form-control form-select" multiple aria-label="multiple select example" required>
                        <?php foreach ($languages as $key => $value) : ?>
                            <option value="<?=$key?>"
                                <?php if(array_search($value, $m_languages)):?>
                                    selected
                                <?php endif; ?>
                            ><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="my-3">
                    <h5>Select Genres</h5>
                    <select name="genres[]" class="form-control form-select" multiple aria-label="multiple select example" required> 
                        <?php foreach ($genres as $key => $value) : ?>
                            <option value="<?=$key?>"
                                <?php if(array_search($value, $m_genres)):?>
                                    selected
                                <?php endif; ?>
                            ><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input class="btn btn-primary btn-block" type="submit" value="Update">
            </form>
            
            <?php if(isset($success) && !$success) :?>
                <div class="alert alert-danger">
                    <strong>Failure!</strong> Movie not updated
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>