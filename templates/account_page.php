<div class="container my-5">
    <div id="profile-header" class="p-2 p-sm-3 p-lg-5 bg-dark shadow rounded">
        <div class="row align-items-center">
            <div class="col-12 d-flex flex-row justify-content-center col-md profile-avatar">
                <img src=<?= "https://ui-avatars.com/api/?name=$account_user->username&rounded=true&background=888&color=fff&size=128" ?>
                    style="width: clamp(100px, 22vw, 190px); height: clamp(100px, 22vw, 190px);" class="rounded-circle">
            </div>
            <div class="col-12 d-flex flex-row justify-content-center col-md profile-name">
                <div class="h1 text-white"><?= $account_user->username ?></div>
            </div>
            <div class="col-12 col-md d-flex flex-row align-items-center justify-content-center justify-md-content-end">
                <?php if($is_user_self) : ?>
                <a class="btn btn-lg h5 btn-primary" href="/logout.php">
                    Logout
                </a>
                <?php endif; ?>
                <?php if($show_follow_unfollow) : ?>
                <div class="follow-btn ml-3">
                    <?php if (!$isFollowing) : ?>
                    <a class="btn btn-lg h5 btn-primary" href="follow.php?profile_id=<?= $account_user->id ?>&follow=1">
                        Follow
                    </a>
                    <?php else : ?>
                    <a class="btn btn-lg h5 btn-primary" href="follow.php?profile_id=<?= $account_user->id ?>&follow=0">
                        Unfollow
                    </a>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <?php if($is_logged_in && !$is_user_self && !$is_admin) : ?>
                <button type="button" data-toggle="modal" data-target="#report-user-modal"
                    class="ml-2 btn btn-lg h5 btn-info d-flex flex-row align-items-center">Report</button>
                <?php endif; ?>
                <?php if(!$is_user_self && $is_admin) : ?>
                <button type="button" data-toggle="modal" data-target="#warn-user-modal"
                    class="ml-2 btn btn-lg h5 btn-warning d-flex flex-row align-items-center">Warn</button>
                <?php if($is_disabled) : ?>
                <a href="/admin/enable_user.php?id=<?=$account_user_id?>"
                    class="ml-2 btn btn-lg h5 btn-success d-flex flex-row align-items-center">Enable</a>
                <?php else :?>
                <button type="button" data-toggle="modal" data-target="#disable-user-modal"
                    class="ml-2 btn btn-lg h5 btn-danger d-flex flex-row align-items-center">Disable</button>
                <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="d-flex flex-row align-items-center justify-content-center my-3">
            <span class="d-flex flex-row bg-white rounded p-1 p-md-3">
                <div class="followers">
                    <span class="badge badge-primary h5">Followers</span>
                    <div class="text-right text-secondary h5"><?= $follower_count ?></div>
                </div>
                <div class="followings mx-1 mx-md-3">
                    <span class="badge badge-secondary h5">Followings</span>
                    <div class="text-right text-secondary h5"><?= $following_count ?></div>
                </div>
            </span>
        </div>
    </div>
    <div class="profile-nav mt-5">
        <ul class="nav nav-pills pt-2 pb-1 px-3 rounded" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link h5 active" id="review-tab" data-toggle="tab" href="#review" role="tab"
                    aria-controls="review" aria-selected="true">Reviews</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link" id="custom-list-tab" data-toggle="tab" href="#custom-list" role="tab"
                    aria-controls="custom-list" aria-selected="false">Custom Lists</a>
            </li>
            <?php if($is_user_self) : ?>
            <li class="nav-item h5">
                <a class="nav-link h5" id="watchlist-tab" data-toggle="tab" href="#watchlist" role="tab"
                    aria-controls="watchlist" aria-selected="false">Watchlist</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link" id="followers-tab" data-toggle="tab" href="#followers" role="tab"
                    aria-controls="followers" aria-selected="false">Followers</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link" id="followings-tab" data-toggle="tab" href="#followings" role="tab"
                    aria-controls="followings" aria-selected="false">Following</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link h5" id="starred-list-tab" data-toggle="tab" href="#starred-list" role="tab"
                    aria-controls="starred-list" aria-selected="false">Starred Lists</a>
            </li>
            <?php endif; ?>
        </ul>

    </div>
    <div class="tab-content mt-5" id="myTabContent">
        <div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="review-tab">
            <h3>Reviews</h3>
            <hr>

            <?php foreach ($reviews as $review) : ?>

            <div class="my-4 w-100 w-md-75">
                <div class="p-1 p-md-3 container shadow-sm border bg-white">
                    <div class="mb-3 p-1 p-md-3 bg-dark rounded">
                        <a href="/movie.php?id=<?= $review->movie_id ?>" style="text-decoration: none;">
                            <div class="d-flex flex-row">
                                <div class="flex-grow-1">
                                    <div class="h3 text-white"><?= $review->movie_name ?></div>
                                    <span class="h5 text-secondary">(<?= $review->release_year ?>)</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-1 p-md-3 border border-primary shadow-sm rounded">
                                <div class="d-flex flex-row align-items-center mt-2 mb-3">
                                    <span class="badge badge-primary py-2">Rating</span>
                                    <span class="ml-2 font-weight-bold  ">
                                        <?= $review->rating ?>
                                    </span>
                                </div>
                                <div class="h5">
                                    <?= getReviewString(htmlspecialchars($review->review)) ?>
                                </div>
                                <div>
                                    <a href="/review_page.php?id=<?= $review->id ?>"
                                        class="mt-2 btn btn-sm h5 btn-secondary btn">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-2 d-flex flex-row align-items-center">
                        <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                            <span class="material-icons mr-2">
                                thumbs_up_down
                            </span>
                            <?= $review->vote_count ?>
                        </span>
                        <span class="text-secondary">
                            <small>
                                Posted On
                                <?= getDateTimeString($review->posted_at) ?>
                            </small>
                        </span>
                    </div>
                </div>
                <hr class="my-4">
            </div>
            <?php endforeach; ?>
        </div>

        <div class="tab-pane fade" id="watchlist" role="tabpanel" aria-labelledby="watchlist-tab">
            <h3>You want to see <?= count($watchlist) ?> films</h3>
            <hr>
            <ul class="list-group">
                <?php foreach ($watchlist as $movie) : ?>
                <a class="btn list-group-item d-flex flex-row align-items-center"
                    href="/movie.php?id=<?= $movie->movie_id ?>" style="text-decoration: none;">
                    <span>
                        <img class="rounded border" style="height: 100%; width: clamp(60px, 22vw, 120px)"
                            <?php if($movie->show_poster_from_imdb == 't'):?> data-imdb-id=<?= $movie->imdb_id ?>
                            <?php endif;?> src="<?= $movie->movie_poster_name ?>"
                            alt="<?= $movie->movie_name ?> Poster">
                    </span>
                    <div class="ml-5 align-self-start d-flex flex-column align-items-start">
                        <div class="h3"><?= $movie->movie_name ?></div>
                        <div class="h5 text-secondary">(<?= $movie->release_year ?>)</div>
                    </div>
                </a>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php if($is_user_self) : ?>
        <div class="tab-pane fade" id="followers" role="tabpanel" aria-labelledby="followers-tab">
            <h3><?= count($followers) ?> people follow you</h3>
            <hr class="my-3">
            <ul class="list-group">
                <?php foreach ($followers as $follower) : ?>
                <div class="list-group-item">
                    <div class="follower-row row">
                        <a href="/account.php?id=<?= $follower->id ?>">
                            <div class="ml-3" style="float: left;">
                                <img src=<?= "https://ui-avatars.com/api/?name=$follower->username&rounded=true&background=888&color=fff&size=128" ?>
                                    width="30" height="30" class="rounded-circle">
                            </div>

                            <div class="my-auto ml-3" style="display: inline-block;">
                                <h6 class="my-auto h5"><?= $follower->username?></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="tab-pane fade" id="followings" role="tabpanel" aria-labelledby="followings-tab">
            <h3>You follow <?= count($followings) ?> people</h3>
            <hr class="my-3">
            <ul class="list-group">
                <?php foreach ($followings as $following) : ?>
                <div class="list-group-item">
                    <div class="following-row row">
                        <a href="/account.php?id=<?= $following->id ?>">
                            <div class="ml-3" style="float: left;">
                                <img src=<?= "https://ui-avatars.com/api/?name=$following->username&rounded=true&background=888&color=fff&size=128" ?>
                                    width="30" height="30" class="rounded-circle">
                            </div>

                            <div class="my-auto ml-3" style="display: inline-block;">
                                <h6 class="my-auto h5"><?= $following->username?></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="tab-pane fade" id="starred-list" role="tabpanel" aria-labelledby="starred-list-tab">
            <div class="d-flex flex-row">
                <div class="h3 flex-grow-1">Starred Lists</div>
            </div>
            <hr>
            <ul class="list-group">
                <?php foreach ($starred_lists as $list) : ?>
                <li class="list-group-item d-flex flex-row align-items-center">
                    <a href="/custom_list.php?id=<?= $list['id'] ?>"
                        class="btn btn-lg btn-block flex-grow-1 d-flex flex-row justify-content-start mr-3">
                        <?= $list['name'] ?>
                        (<?= $list['movie_count'] ?>)
                    </a>
                    <div class="col-1 d-flex flex-row align-items-center">
                        <span class="material-icons mr-1">
                            star
                        </span>
                        <?=$list['star_count']?>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php endif; ?>

        <div class="tab-pane fade" id="custom-list" role="tabpanel" aria-labelledby="custom-list-tab">
            <div class="d-flex flex-row">
                <div class="h3 flex-grow-1">Custom Lists</div>
                <?php if($is_user_self) : ?>
                <div>
                    <a href="/create_custom_list.php" class="btn btn-success d-flex flex-row align-items-center">
                        New List
                        <span class="material-icons ml-2">
                            add
                        </span>
                    </a>
                </div>
                <?php endif; ?>
            </div>
            <hr>
            <ul class="list-group">
                <?php foreach ($custom_lists as $list) : ?>
                <li class="list-group-item d-flex flex-row align-items-center">
                    <?php if($is_user_self) : ?>
                    <?php if($list['is_public'] == 't') :?>
                    <span class="badge badge-warning">Public
                    </span>
                    <?php else :?>
                    <span class="badge badge-info">Private
                    </span>
                    <?php endif;?>
                    <?php endif;?>

                    <a href="/custom_list.php?id=<?= $list['id'] ?>"
                        class="btn btn-lg btn-block flex-grow-1 d-flex flex-row justify-content-start mr-3">
                        <?= $list['name'] ?>
                        (<?= $list['movie_count'] ?>)
                    </a>
                    <div class="col-1 d-flex flex-row align-items-center">
                        <span class="material-icons mr-1">
                            star
                        </span>
                        <?=$list['star_count']?>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div id="snackbar"></div>
    <div class="modal fade" id="report-user-modal" tabindex="-1" role="dialog" aria-labelledby="report-user-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="report-user-modal-label">Report User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php foreach ($report_reasons as $reason) { ?>
                    <input type="radio" name="reason" id="reason<?=$reason->id?>" value="<?=$reason->id?>">
                    <label for="reason<?=$reason->id?>"><?= $reason->report_reason ?></label>
                    <br />
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" onclick="submit_report()">Report</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="warn-user-modal" tabindex="-1" role="dialog" aria-labelledby="warn-user-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="warn-user-modal-label">Warn User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="warn_reason">Reason</label>
                    <br />
                    <textarea name="warn" id="warn_reason" cols="30" rows="3"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" onclick="warn_user()">Warn</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="disable-user-modal" tabindex="-1" role="dialog"
        aria-labelledby="disable-user-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="disable-user-modal-label">Disable User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/admin/disable_user.php?id=<?=$account_user_id?>" method="post">
                    <div class="modal-body">
                        <label for="disable_reason">Reason</label>
                        <br />
                        <textarea name="disable" id="disable_reason" cols="30" rows="3" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-info" onclick="warn_user()">Disable</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-pills a').click(function(e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
});

function showSnackBar(html) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
    x.innerHTML = html;
    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function submit_report() {
    let reason_id = document.querySelector('input[name="reason"]:checked');
    if (!reason_id)
        return;
    reason_id = reason_id.value;
    console.log(reason_id);
    $("#report-user-modal").modal("hide");
    fetch(`/ajax/report_user.php?reason_id=${reason_id}&resource_id=<?=$account_user->id?>`)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.success) {
                showSnackBar(
                    `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Reported user
                        </div>`);

            } else {
                $(e.target).val(false);
                showSnackBar(
                    `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt report user
                        </div>`);
            }
        }).catch(err => console.error(err))
}

function warn_user() {
    let reason = document.getElementById("warn_reason").value;
    if (!reason)
        return;
    console.log(reason);
    $("#warn-user-modal").modal("hide");
    fetch(`/ajax/warn_user.php?reason=${reason}&id=<?=$account_user->id?>`)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.success) {
                showSnackBar(
                    `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Warned user
                        </div>`);

            } else {
                $(e.target).val(false);
                showSnackBar(
                    `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt warn user
                        </div>`);
            }
        }).catch(err => console.error(err))
}
</script>


<style>
/* The snackbar - position it at the bottom*/
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    position: fixed;
    z-index: 1;
    left: 20%;
    bottom: 30px;
}

/* Show the snackbar on function call (class added with JavaScript) */
#snackbar.show {
    visibility: visible;
    /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@-webkit-keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}

@keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}
</style>