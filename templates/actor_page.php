<div class="container my-5">
    <img style="height:200px;" data-actor="<?=$actor['actor']?>" src="/images/default_profile.png">
    <h2><?=$actor['actor']?></h2>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse1">Acted In <?= count($movies) ?>
                        movies </a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($movies as $movie) : ?>
                    <li class="list-group-item">
                        <a href="/movie.php?id=<?= $movie->movie_id ?>">
                            <span style="float: left;">
                                <img <?php if($movie->show_poster_from_imdb == 't'):?>
                                    data-imdb-id=<?= $movie->imdb_id ?> <?php endif;?> width="120" height="150"
                                    src="<?= $movie->movie_poster_name ?>" alt="<?= $movie->movie_name ?> Poster">
                            </span>
                            <span class="ml-3 h3">
                                <?= $movie->movie_name ?>
                            </span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse2">Co-Stars</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($costars as $c) : ?>
                    <li class="list-group-item">
                        <a href="/actor.php?id=<?= $c['actor_id'] ?>">
                            <span class="h5"><?= $c['actor'] ?></span>
                        </a>
                        <a href="/ref/actor_actor.php?id1=<?=$actor['id']?>&id2=<?=$c['actor_id']?>"
                            style="float: right;">Worked Together : <?= $c['count'] ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse3">Directors worked with</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($directors as $d) : ?>
                    <li class="list-group-item">
                        <a href="/director.php?id=<?= $d['director_id'] ?>">
                            <span class="h5"><?= $d['director'] ?></span>
                        </a>
                        <a href="/ref/actor_director.php?a=<?=$actor['id']?>&d=<?= $d['director_id'] ?>"
                            style="float: right;">Worked Together : <?= $d['count'] ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse4">Genres worked in</a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($genresWorkedIn as $g) : ?>
                    <li class="list-group-item">
                        <span class="h5"><?= $g['genre_name'] ?></span>
                        <a href="/ref/actor_genre.php?a=<?=$actor['id']?>&g=<?= $g['genre_id'] ?>"
                            style="float: right;">Worked In : <?= $g['count'] ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>