<div class="container my-5">
    <div class="bg-dark text-white px-5 py-3 rounded shadow mb-5">
        <h2>User Review Feed</h2>
    </div>
    <div class="feed">
        <?php foreach ($feed as $review) : ?>
        <div class="my-4 w-100 feed-review" data-posted-at="<?= $review->posted_at?>">
            <div class="p-2 p-lg-3 container shadow-sm border bg-white">
                <div class="mb-3 p-1 p-md-3 bg-dark rounded">
                    <a href="/movie.php?id=<?= $review->movie_id ?>" style="text-decoration: none;">
                        <div class="d-flex flex-row">
                            <div class="flex-grow-1">
                                <div class="h3 text-white"><?= $review->movie_name ?></div>
                                <span class="h5 text-secondary">(<?= $review->release_year ?>)</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="row">
                    <div class="col-12 my-1 col-lg-2 my-lg-0">
                        <a class="btn btn-light d-flex flex-row align-items-center"
                            href="/account.php?id=<?=$review->user_id?>">
                            <img src=<?= "https://ui-avatars.com/api/?name=$review->username&rounded=true&background=888&color=fff&size=128" ?>
                                width="30" height="30" class="rounded-circle">
                            <span class="ml-3">
                                <?= $review->username ?>
                            </span>
                        </a>
                    </div>
                    <div class="col-12 col-lg-10">
                        <div class="p-2 p-lg-3 border border-primary shadow-sm rounded">
                            <div class="d-flex flex-row align-items-center mt-2 mb-3">
                                <span class="badge badge-primary py-2">Rating</span>
                                <span class="ml-2 font-weight-bold  ">
                                    <?= $review->rating ?>
                                </span>
                            </div>
                            <div class="h5">
                                <?= getReviewString(htmlspecialchars($review->review)) ?>
                            </div>
                            <div>
                                <a href="/review_page.php?id=<?= $review->id ?>"
                                    class="mt-2 btn btn-sm h5 btn-secondary btn">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row align-items-center my-2">
                    <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                        <span class="material-icons mr-2">
                            thumbs_up_down
                        </span>
                        <?= $review->vote_count ?>
                    </span>
                    <span class="text-secondary">
                        <small>
                            Posted On
                            <?= getDateTimeString($review->posted_at) ?>
                        </small>
                    </span>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<script>
let feed_processing = false,
    feed_end = false;

$(document).scroll(function(e) {
    if (feed_processing || feed_end)
        return;
    // grab the scroll amount and the window height
    var scrollAmount = $(window).scrollTop();
    var documentHeight = $(document).height();

    // calculate the percentage the user has scrolled down the page
    var scrollPercent = (scrollAmount / documentHeight) * 100;

    if (scrollPercent > 50) {
        // run a function called doSomething
        doSomething();
    }

    function doSomething() {
        feed_processing = true;
        console.log("reached to 50%");
        let last_posted_at = document.querySelector(".feed-review:last-of-type").dataset.postedAt;
        console.log("last posted", last_posted_at);
        fetch('/ajax/feed_on_scroll.php?last_posted_at=' + last_posted_at)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (!res.success) {
                    feed_end = true;
                    return;
                }
                if (res.data.length < 10) {
                    feed_end = true;
                }
                let feed = document.querySelector(".feed");
                let str = ""
                for (const d of res.data) {
                    console.log(d)
                    str += createReviewDiv(d);
                }
                feed.innerHTML += str;
                console.log("done");
                feed_processing = false;
            })
            .catch(e => {
                feed_end = true;
            })
    }

});

function createReviewDiv(review) {
    return `
    <div class="my-4 w-100 feed-review" data-posted-at="${review.posted_at}">
        <div class="p-2 p-lg-3 container shadow-sm border bg-white">
            <div class="mb-3 p-1 p-md-3 bg-dark rounded">
                <a href="/movie.php?id=${review.movie_id}" style="text-decoration: none;">
                    <div class="d-flex flex-row">
                        <div class="flex-grow-1">
                            <div class="h3 text-white">${review.movie_name}</div>
                            <span class="h5 text-secondary">(${review.release_year})</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row">
                <div class="col-12 my-1 col-lg-2 my-lg-0">
                    <a class="btn btn-light d-flex flex-row align-items-center"
                        href="/account.php?id=${review.user_id}">
                        <img src=${`https://ui-avatars.com/api/?name=${review.username}&rounded=true&background=888&color=fff&size=128`}
                            width="30" height="30" class="rounded-circle">
                        <span class="ml-3">
                            ${review.username}
                        </span>
                    </a>
                </div>
                <div class="col-12 col-lg-10">
                    <div class="p-2 p-lg-3 border border-primary shadow-sm rounded">
                        <div class="d-flex flex-row align-items-center mt-2 mb-3">
                            <span class="badge badge-primary py-2">Rating</span>
                            <span class="ml-2 font-weight-bold  ">
                            ${review.rating}
                            </span>
                        </div>
                        <div class="h5">
                            ${js_getReviewString(js_htmlspecialchars(review.review)) }
                        </div>
                        <div>
                            <a href="/review_page.php?id=${review.id}"
                                class="mt-2 btn btn-sm h5 btn-secondary btn">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row align-items-center my-2">
                <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                    <span class="material-icons mr-2">
                        thumbs_up_down
                    </span>
                    ${review.vote_count}
                </span>
                <span class="text-secondary">
                    <small>
                        Posted On
                        ${js_getDateTimeString(review.posted_at)}
                    </small>
                </span>
            </div>
        </div>
    </div>
    `;
}

function js_getReviewString(text) {
    maxLen = 300;
    if (text == "") return "<i>No review</i>";
    if (text.length <= maxLen) return text;
    return text.substr(0, maxLen) + "....";
}

function js_getDateTimeString(x) {
    let date = new Date(x);
    return date.toUTCString();
}

function js_htmlspecialchars(x) {
    return String(x).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
</script>