<div class="container my-5">
    <h2>Disabled Users</h2>

    <?php if(count($disabled_users)) : ?>
    <div class="list-group">
        <?php foreach ($disabled_users as $user):?>
        <div class='list-group-item'>
            <a class="btn text-primary col-3 text-left" href=<?= '/account.php?id='.$user->user_id?>>
                <b><?=$user->username?></b>
            </a>
            <span><?= $user->reason?></span>
            <span style='float: right;'>
                <a href="/admin/enable_user.php?id=<?=$user->user_id?>&redirect=/admin/disabled_users.php"
                    class="btn btn-outline-success mr-4">Enable</a>
                <?= getDateTimeString($user->disabled_at) ?>
            </span>
        </div>
        <?php endforeach; ?>
    </div>
    <?php else: ?>
    <h4>No Disabled Users</h4>
    <?php endif; ?>
</div>
</div>