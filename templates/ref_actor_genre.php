<div class="container my-5">
    <a href="/actor.php?id=<?=$actor['id']?>">
        <img style="height:200px;" data-actor="<?=$actor['actor']?>" src="/images/default_profile.png">
        <h2><?=$actor['actor']?></h2>
    </a>

    <div class="h2 my-3">
        Worked In <?= count($movies)?> movies with genre: <?= $genre ?>
    </div>

    <?php foreach ($movies as $movie) : ?>
    <?php 
        $card = new Template('templates/components/movie_row_item.php');
        $card->movie_id = $movie->id;
        $card->movie_imdb_id = $movie->imdb_id;
        $card->movie_poster_name = $movie->movie_poster_name;
        $card->movie_name = $movie->movie_name;
        $card->movie_show_poster_from_imdb = $movie->show_poster_from_imdb;
        $card->renderHTML();
    ?>
    <?php endforeach; ?>
</div>