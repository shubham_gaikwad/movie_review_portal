<div class="container my-5">
    <div class="card" style="width:70%">
        <div class="card-header h4">
            Create Custom Movie List
        </div>
        <div class="card-body">
            <form action="create_custom_list.php?redirect=<?=$redirect?>" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="list_name" required placeholder="List Name" required>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked name="private">
                    <label class="form-check-label" for="exampleCheck1">Make Private</label>
                </div>
                <input class="btn btn-primary btn-block" type="submit" value="Create">
            </form>
        </div>
    </div>
</div>