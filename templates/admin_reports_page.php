<div class="container my-5">
    <h2>Reports</h2>
    <div class="profile-nav mt-5">
        <ul class="nav nav-pills pt-2 pb-1 px-3 rounded" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link h5 active" id="unseen-tab" data-toggle="tab" href="#unseen" role="tab"
                    aria-controls="unseen" aria-selected="true">Unseen</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link" id="ignored-tab" data-toggle="tab" href="#ignored" role="tab"
                    aria-controls="ignored" aria-selected="false">Ignored</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab"
                    aria-controls="completed" aria-selected="false">Completed</a>
            </li>
        </ul>
    </div>

    <div class="tab-content mt-5" id="myTabContent">
        <div class="tab-pane fade show active" id="unseen" role="tabpanel" aria-labelledby="unseen-tab">
            <div class="list-group">
                <?php foreach ($unseen as $report):?>
                <div class='list-group-item'>
                    <?php if($report->resource_type == 'REVIEW') :?>
                    <a href=<?= '/review_page.php?id='.$report->resource_id?>>
                        <span class="badge badge-info col-1">Review</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'COMMENT'):?>
                    <a href='/comment.php?id=<?=$report->resource_id?>'>
                        <span class="badge badge-info col-1">Comment</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'MOVIE'):?>
                    <a href=<?= '/movie.php?id='.$report->resource_id?>>
                        <span class="badge badge-danger col-1">Movie</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'USER'):?>
                    <a href=<?= '/account.php?id='.$report->resource_id?>>
                        <span class="badge badge-warning col-1">User</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php endif;?>


                    <span style='float: right;'>
                        <?= getDateTimeString($report->reported_at) ?>
                        <a href="/admin/report_complete.php?report_id=<?= $report->id?>"
                            class="btn btn-sm btn-success ml-3">Complete</a>
                        <a href="/admin/report_ignore.php?report_id=<?= $report->id?>"
                            class="btn btn-sm btn-danger ml-3">Ignore</a>
                    </span>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="tab-pane fade" id="ignored" role="tabpanel" aria-labelledby="ignored-tab">
            <div class="list-group">
                <?php foreach ($ignored as $report):?>
                <div class='list-group-item'>
                    <?php if($report->resource_type == 'REVIEW') :?>
                    <a href=<?= '/review_page.php?id='.$report->resource_id?>>
                        <span class="badge badge-info col-1">Review</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'COMMENT'):?>
                    <a href='/comment.php?id=<?=$report->resource_id?>'>
                        <span class="badge badge-info col-1">Comment</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'MOVIE'):?>
                    <a href=<?= '/movie.php?id='.$report->resource_id?>>
                        <span class="badge badge-danger col-1">Movie</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'USER'):?>
                    <a href=<?= '/account.php?id='.$report->resource_id?>>
                        <span class="badge badge-warning col-1">User</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php endif;?>


                    <span style='float: right;'>
                        <?= getDateTimeString($report->reported_at) ?>
                        <a href="/admin/report_complete.php?report_id=<?= $report->id?>"
                            class="btn btn-sm btn-success ml-3">Complete</a>
                    </span>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
            <div class="list-group">
                <?php foreach ($completed as $report):?>
                <div class='list-group-item'>
                    <?php if($report->resource_type == 'REVIEW') :?>
                    <a href=<?= '/review_page.php?id='.$report->resource_id?>>
                        <span class="badge badge-info col-1">Review</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'COMMENT'):?>
                    <a href='/comment.php?id=<?=$report->resource_id?>'>
                        <span class="badge badge-info col-1">Comment</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'MOVIE'):?>
                    <a href=<?= '/movie.php?id='.$report->resource_id?>>
                        <span class="badge badge-danger col-1">Movie</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php elseif($report->resource_type == 'USER'):?>
                    <a href=<?= '/account.php?id='.$report->resource_id?>>
                        <span class="badge badge-warning col-1">User</span>
                        <b><?= $report->report_reason?></b>
                    </a>
                    <?php endif;?>


                    <span style='float: right;'>
                        <?= getDateTimeString($report->reported_at) ?>
                    </span>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>