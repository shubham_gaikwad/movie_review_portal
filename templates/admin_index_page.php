<div class="container my-5">
    <h1> Welcome admin </h1>
<!-- <a class="btn btn-primary" href="/admin/insert_movie.php">Insert New Movie</a> -->
    <div >
        <a class="btn btn-lg btn-outline-primary h4 btn-block" href="/admin/reports.php">Go to Reports</a>
    </div>
    <div >
        <a class="btn btn-lg btn-outline-primary h4 btn-block" href="/admin/disabled_users.php">Disabled Users</a>
    </div>
    <div class="row my-5">
        <div class="col" >
            <div class="card" style="width:70%">
                <div class="card-header h4">
                    Insert Language
                </div>
                <div class="card-body">
                    <form action="/admin/insert_language.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="language" value="" required placeholder="Language">
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Insert">                    
                    </form>
                    <?php if(get($_GET['ins_lang'])) :?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> Language inserted
                        </div>
                    <?php elseif(get($_GET['err_ins_lang'])) :?>
                        <div class="alert alert-danger">
                            <strong>Failure!</strong> Language not inserted
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card" style="width:70%">
                <div class="card-header h4">
                    Insert Genre
                </div>
                <div class="card-body">
                    <form action="/admin/insert_genre.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="genre"  value="" required placeholder="Genre">
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Insert">                    
                    </form>
                    <?php if(get($_GET['ins_genre'])) :?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> Genre inserted
                        </div>
                    <?php elseif(get($_GET['err_ins_genre'])) :?>
                        <div class="alert alert-danger">
                            <strong>Failure!</strong> Genre not inserted
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5">
        <div class="col" >
            <div class="card" style="width:70%">
                <div class="card-header h4">
                    Insert Actor
                </div>
                <div class="card-body">
                    <form action="/admin/insert_actor.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="actor" value="" required placeholder="Actor">
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Insert">                    
                    </form>
                    <?php if(get($_GET['ins_actor'])) :?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> Actor inserted
                        </div>
                    <?php elseif(get($_GET['err_ins_actor'])) :?>
                        <div class="alert alert-danger">
                            <strong>Failure!</strong> Actor not inserted
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card" style="width:70%">
                <div class="card-header h4">
                    Insert Director
                </div>
                <div class="card-body">
                    <form action="/admin/insert_director.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="director"  value="" required placeholder="Director">
                        </div>
                        <input class="btn btn-primary btn-block" type="submit" value="Insert">                    
                    </form>
                    <?php if(get($_GET['ins_director'])) :?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> Director inserted
                        </div>
                    <?php elseif(get($_GET['err_ins_director'])) :?>
                        <div class="alert alert-danger">
                            <strong>Failure!</strong> Director not inserted
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card" style="width:70%">
        <div class="card-header h4">
            Insert Movie
        </div>
        <div class="card-body">
            <form action="insert_movie.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" class="form-control" name="movie_name" required placeholder="Movie Name">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" name="release_year" required placeholder="Release Year">
                </div>
                <div class="form-group">
                    <textarea name="summary" class="form-control" cols="30" rows="10" placeholder="Summary"></textarea>
                </div>        
                <div class="my-3">
                    <h5>Select Languages</h5>
                    <select name="languages[]" class="form-control form-select" multiple aria-label="multiple select example" required>
                        <?php foreach ($languages as $key => $value) : ?>
                                <option value="<?=$key?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="my-3">
                    <h5>Select Genres</h5>
                    <select name="genres[]" class="form-control form-select" multiple aria-label="multiple select example" required> 
                        <?php foreach ($genres as $key => $value) : ?>
                                <option value="<?=$key?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input class="btn btn-primary btn-block" type="submit" value="Insert">
            </form>
            
            <?php if(get($_GET['err_ins_movie'])) :?>
                <div class="alert alert-danger">
                    <strong>Failure!</strong> Movie not inserted
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>