<div class="container my-5">
    <div class="bg-dark text-white px-5 py-3 rounded shadow mb-5">
        <h2>Top Lists</h2>
    </div>

    <ul class="list-group">
        <?php foreach ($top_lists as $list) : ?>
        <li class="list-group-item d-flex flex-row align-items-center">
            <a href="/custom_list.php?id=<?= $list['id'] ?>"
                class="btn btn-lg btn-block flex-grow-1 d-flex flex-row justify-content-start mr-3">
                <?= $list['name'] ?>
                (<?= $list['movie_count'] ?>)
            </a>
            <div class="col-1 d-flex flex-row align-items-center">
                <span class="material-icons mr-1">
                    star
                </span>
                <?=$list['star_count']?>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</div>