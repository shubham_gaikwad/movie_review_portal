<form class="row form-inline my-2 my-lg-0" id="nav-search-form" action="search.php" method="get"
    style="position:relative;">
    <input class="ml-3 col-xs-12 col-lg-6 form-control" type="search" style="width: max-content;"
        placeholder="<?= isset($search_movies_only)?'Search Movie' : 'Browse'?>" name="search" value="<?= $search ?>"
        id="nav-search-input">
    <button class="ml-3 btn btn-outline-primary h5 my-2 my-sm-0" type="submit" style="width: fit-content;">Search
        Movie</button>
    <ul style="position:absolute; z-index: 10; top:50px; max-height: 300px;  overflow-y: auto;" class="list-group w-100"
        id="nav-suggesstion-box"></ul>

    <?php if(isset($search_movies_only)) :?>
    <div class="col-12 ml-2">
        <div class="row">
            <div class="dropdown">
                <button type="button" class="btn btn-outline-secondary my-2 ml-2 dropdown-toggle h5"
                    data-toggle="dropdown" id="genre-btn">
                    Genres
                </button>
                <div class="dropdown-menu" style="max-height: 300px;  overflow-y: auto; z-index:100000;">
                    <div class="ml-2">
                        <input type="radio" name="genre" value="" id="g-">
                        All
                    </div>
                    <?php foreach ($genres as $key => $value) : ?>
                    <div class="ml-2">
                        <input type="radio" name="genre" value="<?= $key ?>" id="g-<?= $key ?>">
                        <?= $value ?>
                    </div>
                    <?php endforeach; ?>
                </div>
                <script>
                $("#g-<?= $genre_id ?>").prop("checked", true);
                </script>
            </div>
            <div class="dropdown">
                <button type="button" class="btn btn-outline-secondary my-2 ml-2 dropdown-toggle h5"
                    style="max-height: 300px;  overflow-y: auto; z-index:100000;" data-toggle="dropdown" id="genre-btn">
                    Languages
                </button>
                <div class="dropdown-menu" style="max-height: 300px;  overflow-y: auto; z-index:100000;">
                    <div class="ml-2">
                        <input type="radio" name="lang" value="" id="l-">
                        All
                    </div>
                    <?php foreach ($languages as $key => $value) : ?>
                    <div class="ml-2">
                        <input type="radio" name="lang" value="<?= $key ?>" id="l-<?= $key ?>">
                        <?= $value ?>
                    </div>
                    <?php endforeach; ?>
                </div>
                <script>
                $("#l-<?= $lang_id ?>").prop("checked", true);
                </script>
            </div>
            <div class="dropdown">
                <button type="button" class="btn btn-outline-secondary my-2 mx-2 dropdown-toggle h5"
                    data-toggle="dropdown" id="genre-btn">
                    Sort by
                </button>
                <div class="dropdown-menu" style="max-height: 600px; overflow-y: auto;"
                    style="max-height: 300px;  overflow-y: auto; z-index:100000;">
                    <div class="ml-2">
                        <input type="radio" name="sort" value="1" id="s-1">
                        Name: A to Z
                    </div>
                    <div class="ml-2">
                        <input type="radio" name="sort" value="2" id="s-2">
                        Name: Z to A
                    </div>
                    <div class="ml-2">
                        <input type="radio" name="sort" value="3" id="s-3">
                        Year: Low to High
                    </div>
                    <div class="ml-2">
                        <input type="radio" name="sort" value="4" id="s-4">
                        Year: High to Low
                    </div>
                </div>
                <script>
                $("#s-<?= $sort ?>").prop("checked", true);
                </script>
            </div>
        </div>

    </div>
    <?php endif;?>
</form>
<?php if(!isset($search_movies_only)) :?>
<script src="/js/search_actor_director_movie.js"></script>
<?php endif; ?>