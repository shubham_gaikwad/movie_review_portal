<?php
    // $this->movie_id
    // $this->movie_imdb_id
    // $this->movie_poster_name
    // $this->movie_name
?>

<div class="row">
    <a href="/movie.php?id=<?= $this->movie_id ?>">
        <div style="float: left;">
            <img <?php if($this->movie_show_poster_from_imdb == 't'):?> data-imdb-id=<?= $this->movie_imdb_id ?>
                <?php endif;?> width="120" height="150" src="<?= $this->movie_poster_name ?>"
                alt="<?= $this->movie_name ?> Poster">
        </div>
    </a>
    <a href="/movie.php?id=<?= $this->movie_id ?>">
        <div class="ml-3">
            <h2><?= $this->movie_name ?></h2>
        </div>
    </a>
</div>
<hr>