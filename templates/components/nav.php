<nav class="fixed-top navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">WTW</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <div class="row align-items-center justify-content-between w-100">
            <div class="col-xs-12 col-lg-4">
                <ul class="navbar-nav mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/top_lists.php">Top Lists</a>
                    </li>
                    <?php if ($is_logged_in) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/feed.php">Feed</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/notifications.php">Notifications</a>
                    </li>
                    <?php endif; ?>
                    <?php if($is_admin) :?>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/index.php">Admin Console</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php if(!isset($search_movies_only)) :?>

            <div class="col-xs-12 col-lg-5">

                <?php 
                if(!isset($languages)) {
                    $languages = LanguageDao::getAll();
                }
                if(!isset($genres)) {
                    $genres = GenreDao::getAll();
                }
                require_once("templates/components/search_form.php");
            ?>
            </div>
            <?php endif;?>

            <div class="col-xs-12 col-md-2 ml-3 mr-1 p-0">
                <?php if ($is_logged_in) : ?>
                <div class="dropdown mr-1 p-0" style="width: fit-content;">
                    <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown">
                        <img src=<?= "https://ui-avatars.com/api/?name=$user->username&rounded=true&background=888&color=fff&size=128" ?>
                            width="30" height="30" class="rounded-circle">
                        <span class="mx-2 h5"><?= $user->username ?></span>
                    </button>
                    <div class="dropdown-menu">
                        <div class="dropdown-item">
                            <a class="btn btn-outline-primary h5 btn-block" href="/account.php">Profile</a>
                        </div>
                        <div class="dropdown-item">
                            <a class="btn btn-outline-danger h5 btn-block" href="/logout.php">Logout</a>
                        </div>
                    </div>
                </div>
                <?php else : ?>
                <a href="/login.php?redirect=<?=$_SERVER['REQUEST_URI']?>" class="btn h5 btn-primary">Login</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav>
<script>
$('body').css('padding-top', $('.navbar').outerHeight() + 'px');
</script>