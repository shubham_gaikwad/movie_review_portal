<div class="container my-5 border rounded shadow p-2 p-sm-3 p-lg-4">
    <h2 class="rounded shadow-sm bg-dark text-light px-3 py-1">
        Recent <?=$this->language ?> Movies
    </h2>
    <div class="">
        <div class="d-flex flex-row overflow-auto">
            <?php foreach ($this->movies as $movie) : ?>
            <div class="mt-5 mb-2 mx-2">
                <?php 
                        $card = new Template('templates/components/movie_card.php');
                        $card->movie_id = $movie->movie_id;
                        $card->movie_imdb_id = $movie->imdb_id;
                        $card->movie_name = $movie->movie_name;
                        $card->movie_release_year = $movie->release_year;
                        $card->movie_poster_name = $movie->movie_poster_name;
                        $card->movie_review_count = $movie->review_count;
                        $card->movie_avg_rating = $movie->avg_rating;
                        $card->movie_show_poster_from_imdb = $movie->show_poster_from_imdb;
                        $card->renderHTML();
                    ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="mb-2 mt-5">
            <a href="<?=$this->link?>" class="btn btn-outline-dark btn-lg btn-block h5">See more</a>
        </div>
    </div>
</div>