<?php 
    // $this->movie_id
    // $this->movie_imdb_id
    // $this->movie_name
    // $this->movie_release_year
    // $this->movie_poster_name
    // $this->movie_review_count
    // $this->movie_avg_rating
    // $this->movie_show_poster_from_imdb
?>
<a class="btn text-info h-100 p-0 border-0" href="/movie.php?id=<?= $this->movie_id ?>">
    <div style="height: 100%; width: clamp(100px, 22vw, 200px)">
        <img <?php if($this->movie_show_poster_from_imdb == 't'):?> data-imdb-id=<?= $this->movie_imdb_id ?>
            <?php endif;?> class="card-img-top shadow rounded" style="height: auto; width: 100%"
            src="<?= $this->movie_poster_name ?>" alt="Card image">
        <div class="my-3">
            <h5 class="h5"><?= $this->movie_name ?> | <?= $this->movie_release_year ?></h5>
            <div>
                <?php if($this->movie_review_count):?>
                <span data-toggle="tooltip" title="<?= number_format($this->movie_avg_rating, 2); ?> 
                    (<?= $this->movie_review_count ?>)">
                    <?php 
                        $rating = ceil($this->movie_avg_rating) / 2.0;
                        for($i = 1; $i <= $rating; $i++): 
                    ?>
                    <span class="material-icons h5 text-warning">
                        star_rate
                    </span>
                    <?php endfor; ?>
                    <?php if($rating - floor($rating) > 0.3):?>
                    <span class="material-icons h5 text-warning">
                        star_half
                    </span>
                    <?php endif;?>
                </span>
                <?php else:?>
                <div class="pb-1 text-danger"> No ratings available </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</a>