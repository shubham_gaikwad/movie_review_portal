<div class="container my-5">
    <!--  -->

    <div class="jumbotron jumbotron-fluid bg-white border mb-1 px-sm-2 mb-sm-3 px-md-3 mb-md-5 shadow rounded h4">
        <div class="container">
            <div class="bg-dark text-white shadow-sm rounded p-2 p-sm-3 p-md-5">
                <div class="row align-items-center justify-content-between">
                    <div class="col">
                        <h1 class="h1"><?= $movie->movie_name ?>
                        </h1>
                    </div>
                    <?php if($is_admin): ?>
                    <div class="col">
                        <button type="button"
                            class="float-right btn btn-lg h5 btn-danger d-flex flex-row align-items-center"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Admin
                            <span class="ml-2 material-icons">
                                more_vert
                            </span>
                        </button>
                        <div class="dropdown-menu mt-2">
                            <div class="dropdown-item">
                                <a href="/admin/edit_movie_poster.php?id=<?=$movie->id?>"
                                    class="btn btn-outline-secondary h5 w-100">Edit Poster</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="/admin/edit_director.php?id=<?=$movie->id?>"
                                    class="btn btn-outline-secondary h5 w-100">Edit Directors</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="/admin/edit_cast.php?id=<?=$movie->id?>"
                                    class="btn btn-outline-secondary h5 w-100">Edit Cast</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="/admin/update_movie.php?id=<?= $movie->id?>"
                                    class="btn btn-outline-primary h5 w-100">Edit Movie Details</a>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="dropdown-item">
                                <a href="/admin/movie_delete.php?id=<?= $movie->id?>"
                                    class="btn btn-outline-danger h5 w-100">Delete Movie</a>
                            </div>
                        </div>
                    </div>
                    <?php elseif($is_logged_in) :?>
                    <div class="col">
                        <button type="button" data-toggle="modal" data-target="#report-movie-modal"
                            class="float-right btn btn-lg h5 btn-danger d-flex flex-row align-items-center">Report</button>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="row mx-1 justify-content-between">
                    <span class="h2 text-secondary">
                        (<?= $movie->release_year ?>)
                    </span>
                    <?php if ($is_logged_in) : ?>
                    <form action="watchlist.php" method="post">
                        <?php
                    $isInWatchlist = WatchlistDao::isInWatchlist($_SESSION['user']->id, $movie->id);
                    ?>
                        <?php if ($isInWatchlist) : ?>
                        <button class="btn btn-info d-flex flex-row align-items-center" type="submit" name="submit"
                            value="-">
                            <!-- Unfave -->
                            <span class="material-icons h3">
                                bookmark_added
                            </span>
                        </button>
                        <?php else : ?>
                        <button class="btn btn-info d-flex flex-row align-items-center" type="submit" name="submit"
                            value="+">
                            <!-- Fave -->
                            <span class="material-icons h3">
                                bookmark_border
                            </span>
                        </button>
                        <?php endif; ?>
                        <input type="text" name="movie-id" value="<?= $movie->id ?>" hidden>
                        <input type="text" name="redirect" value="/movie.php?id=<?= $movie->id ?>" hidden>
                    </form>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row my-3 my-sm-4 my-md-5 align-items-center">
                <div class="col-7 col-sm-6 col-md-5 col-lg-4">
                    <img <?php if($movie->show_poster_from_imdb == 't'):?> data-imdb-id=<?= $movie->imdb_id ?>
                        <?php endif;?> class="card-img-top rounded border shadow" src="<?= $movie->movie_poster_name ?>"
                        alt="<?= $movie->movie_name ?> Poster">
                </div>
                <div class="col-12 col-md-8 mt-2 mt-3 mb-1 my-md-5">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-3 text-secondary"><b>Director</b></div>
                        <div class="col-12 col-sm-6 p-2 text-sm-right text-uppercase">
                            <?php foreach($m_directors as $key => $name): ?>
                            <a class="btn btn-light btn-sm text-info" href="/director.php?id=<?=$key?>"><?=$name?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- <hr class="my-4"> -->
                    <div class="row my-3 my-sm-4 my-md-5 align-items-center">
                        <div class="col-12 col-sm-3 text-secondary"><b>Ratings </b></div>
                        <div class="col-12 col-sm-6 p-3 text-sm-right text-uppercase">
                            <?= number_format($movie->avg_rating, 2); ?>
                            <span class="badge badge-pill badge-info ml-2"><?= $movie->review_count ?></span>
                        </div>
                    </div>
                    <!-- <hr class="my-4"> -->
                    <div class="row my-3 my-sm-4 my-md-5 align-items-center">
                        <div class="col-12 col-sm-3 text-secondary"><b>Genres </b></div>
                        <div class="col-12 col-sm-6 p-2 text-sm-right text-uppercase">
                            <?php foreach($m_genres as $genre): ?>
                            <span class="btn btn-sm btn-light"><?=$genre?></span>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- <hr class="my-4"> -->
                    <div class="row my-3 my-sm-4 my-md-5 align-items-center">
                        <div class="col-12 col-sm-3 text-secondary"><b>Languages </b></div>
                        <div class="col-12 col-sm-6 p-2 text-sm-right text-uppercase">
                            <?php foreach($m_languages as $language): ?>
                            <span class="btn btn-sm btn-light"><?=$language?></span>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-1">
            <div class="row my-4">
                <div class="col-12 col-sm-3 text-secondary"><b>Summary </b></div>
                <div class="col-sm-8 my-2 my-sm-0">
                    <h4>
                        <?= $movie->summary ?>
                    </h4>
                </div>
            </div>
            <hr class="my-1">
            <div class="my-2 my-sm-3 my-md-5 p-1 p-sm-3 p-md-5 bg-dark rounded shadow-sm"
                style="-webkit-overflow-scrolling: touch;">
                <div class="h2 d-inline-block text-secondary px-2 py-1 rounded"><b>Cast </b></div>
                <div class="d-flex mt-3 flex-row overflow-auto">
                    <?php foreach($m_actors as $key => $name): ?>
                    <a href="/actor.php?id=<?=$key?>" style="text-decoration: none;" class="btn btn-white">
                        <div class="card shadow border-0 mx-0 h-100" style="width: clamp(120px, 20vw, 200px);">
                            <img class="rounded" data-actor="<?=$name?>" src="/images/default_profile_2_by_3.png">
                            <div class="card-body">
                                <span class="h5 text-dark"><?=$name?></span>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($is_logged_in) : ?>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="custom-list-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Add to List
                </button>
                <div class="dropdown-menu w-100" aria-labelledby="custom-list-dropdown">
                    <a class="dropdown-item" href="/create_custom_list.php?redirect=<?=$_SERVER['REQUEST_URI']?>">Create
                        new List</a>
                    <div class="dropdown-divider"></div>
                    <?php foreach ($custom_lists as $list) : ?>
                    <div class="form-check ml-3 dropdown-item">
                        <input type="checkbox" class="form-check-input custom-list-check"
                            id="custom-list-check-<?=$list['id']?>"
                            <?php if(my_array_search($list['id'], $movie_user_custom_list)):?> checked <?php endif;?>
                            data-custom-list-id="<?=$list['id']?>">
                        <a class="form-check-label" for="custom-list-check-<?=$list['id']?>"
                            href="/custom_list.php?id=<?= $list['id'] ?>"><?= $list['name'] ?></a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif ?>
        </div>
    </div>

    <div class="review-section my-3">
        <?php if ($is_logged_in) : ?>
        <?php if (isset($userReview)) : ?>
        <h4>Your Review</h4>

        <div class="my-4 w-100">
            <div class="p-2 p-lg-3 container shadow-sm border bg-white">
                <div class="row">
                    <div class="col-12 my-1 col-lg-2 my-lg-0">
                        <a class="btn btn-light d-flex flex-row align-items-center"
                            href="/account.php?id=<?=$userReview->user_id?>">
                            <img src=<?= "https://ui-avatars.com/api/?name=$userReview->username&rounded=true&background=888&color=fff&size=128" ?>
                                width="30" height="30" class="rounded-circle">
                            <span class="ml-3">
                                <?= $userReview->username ?>
                            </span>
                        </a>
                    </div>
                    <div class="col-12 col-lg-10">
                        <div class="p-2 p-lg-3 border border-primary rounded shadow-sm">
                            <div class="d-flex flex-row align-items-center mt-2 mb-3">
                                <span class="badge badge-primary py-2">Rating</span>
                                <span class="ml-2 font-weight-bold  ">
                                    <?= $userReview->rating ?>
                                </span>
                            </div>
                            <div class="h5">
                                <?= getReviewString(htmlspecialchars($userReview->review)) ?>
                            </div>
                            <div>
                                <a href="/review_page.php?id=<?= $userReview->id ?>"
                                    class="mt-2 btn btn-sm h5 btn-secondary btn">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row align-items-center my-2">
                    <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                        <span class="material-icons mr-2">
                            thumbs_up_down
                        </span>
                        <?= $userReview->vote_count ?>
                    </span>
                    <span class="text-secondary">
                        <small>
                            Posted On
                            <?= getDateTimeString($userReview->posted_at) ?>
                        </small>
                    </span>
                </div>
                <div class="mt-2 d-flex flex-row justify-content-end">
                    <a class="btn btn-outline-warning h5 mx-2 d-flex flex-row align-items-center"
                        href="/review_update.php?id=<?= $userReview->id?>">
                        Edit Review
                        <span class="material-icons ml-2">
                            mode_edit
                        </span></a>
                    <a class="btn btn-outline-danger h5 d-flex flex-row align-items-center"
                        href="/review_delete.php?review_id=<?= $userReview->id?>">
                        Delete Review
                        <span class="material-icons ml-2">
                            delete_outline
                        </span></a>
                </div>
            </div>
        </div>
        <?php elseif($is_disabled) : ?>
        <div class="alert alert-dark">You are disabled by admin</div>
        <?php else: ?>
        <div class="my-3 row review-submit">
            <form class="col-12 col-md-9" action="review_submit.php?movie_id=<?= $movie->id ?>" method="POST">
                <div class="bg-white border shadow rounded p-4">
                    <div class="h4 mt-3">Submit Review</div>
                    <hr class="mt-2 mb-4">
                    <div class="row form-group">
                        <div class="col">
                            <label for="rating" class="h5 text-primary">Rating</label>
                        </div>
                        <div class="col">
                            <select class="h5 form-control" id="rating" name="rating" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                    </div>
                    <textarea class="form-control" name="review_text" cols="50" rows="7"
                        placeholder="Enter review"></textarea>
                    <div class="my-2">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" name="submit" value="Submit   ">
                    </div>
                </div>
            </form>
        </div>
        <?php endif; ?>
        <?php else : ?>
        <a class="my-3 btn btn-lg btn-block btn-outline-dark"
            href="/login.php?redirect=/movie.php?id=<?= $movie->id ?>"> Login
            to
            review, rate and
            watchlater</a>
        <?php endif; ?>

        <div class="mt-5 dropdown">
            <span class="h4">
                Reviews
            </span>
            <button type="button" class="btn btn-outline-info my-2 mx-2 dropdown-toggle" data-toggle="dropdown"
                id="genre-btn">
                Sorted by: <?= ucfirst($sortReviewBy) ?>
            </button>
            <div class="dropdown-menu" style="max-height: 600px; overflow-y: auto;">
                <div class="ml-2">
                    <a class="dropdown-item" href="/movie.php?id=<?=$movie->id?>&sort=recent">Recent</a>
                </div>
                <div class="ml-2">
                    <a class="dropdown-item" href="/movie.php?id=<?=$movie->id?>&sort=popular">Popular</a>
                </div>
            </div>
        </div>

        <div class="reviews-all mt-3">
            <?php if((isset($userReview) && count($m_reviews) > 1) || (!isset($userReview) && count($m_reviews))):?>
            <?php foreach ($m_reviews as $review) : ?>
            <?php if ($review->username !== $user->username) : ?>
            <!-- new -->
            <div class="my-4 w-100">
                <div class="p-2 p-lg-3 container shadow-sm border bg-white">
                    <div class="row">
                        <div class="col-12 my-1 col-lg-2 my-lg-0">
                            <a class="btn btn-light d-flex flex-row align-items-center"
                                href="/account.php?id=<?=$review->user_id?>">
                                <img src=<?= "https://ui-avatars.com/api/?name=$review->username&rounded=true&background=888&color=fff&size=128" ?>
                                    width="30" height="30" class="rounded-circle">
                                <span class="ml-3">
                                    <?= $review->username ?>
                                </span>
                            </a>
                        </div>
                        <div class="col-12 col-lg-10">
                            <div class="p-2 p-lg-3 border border-primary shadow-sm rounded">
                                <div class="d-flex flex-row align-items-center mt-2 mb-3">
                                    <span class="badge badge-primary py-2">Rating</span>
                                    <span class="ml-2 font-weight-bold  ">
                                        <?= $review->rating ?>
                                    </span>
                                </div>
                                <div class="h5">
                                    <?= getReviewString(htmlspecialchars($review->review)) ?>
                                </div>
                                <div>
                                    <a href="/review_page.php?id=<?= $review->id ?>"
                                        class="mt-2 btn btn-sm h5 btn-secondary btn">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row align-items-center my-2">
                        <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                            <span class="material-icons mr-2">
                                thumbs_up_down
                            </span>
                            <?= $review->vote_count ?>
                        </span>
                        <span class="text-secondary">
                            <small>
                                Posted On
                                <?= getDateTimeString($review->posted_at) ?>
                            </small>
                        </span>
                    </div>
                    <?php if($is_admin) :?>
                    <div class="mt-2 d-flex flex-row justify-content-end">
                        <div>
                            <button type="button"
                                class="btn btn-sm btn-outline-danger d-flex flex-row align-items-center"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Admin
                                <span class="ml-2 material-icons">
                                    more_vert
                                </span>
                            </button>
                            <div class="dropdown-menu mt-2">
                                <div class="dropdown-item">
                                    <a href="/review_delete.php?review_id=<?= $review->id?>"
                                        class="btn btn-outline-danger w-100">Delete Review</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php else:?>
            <h5>No reviews</h5>
            <?php endif;?>
        </div>
    </div>
    <!-- The actual snackbar -->
    <div id="snackbar"></div>
    <div class="modal fade" id="report-movie-modal" tabindex="-1" role="dialog"
        aria-labelledby="report-movie-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="report-movie-modal-label">Report Movie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php foreach ($report_reasons as $reason) { ?>
                    <input type="radio" name="reason" id="reason<?=$reason->id?>" value="<?=$reason->id?>">
                    <label for="reason<?=$reason->id?>"><?= $reason->report_reason ?></label>
                    <br />
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" onclick="submit_report()">Report</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function showSnackBar(html) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
    x.innerHTML = html;
    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function submit_report() {
    let reason_id = document.querySelector('input[name="reason"]:checked');
    if (!reason_id)
        return;
    reason_id = reason_id.value;
    console.log(reason_id);
    $("#report-movie-modal").modal("hide");
    fetch(`/ajax/report_movie.php?reason_id=${reason_id}&resource_id=<?=$movie->id?>`)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.success) {
                showSnackBar(
                    `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Reported movie
                        </div>`);

            } else {
                $(e.target).val(false);
                showSnackBar(
                    `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt report movie
                        </div>`);
            }
        }).catch(err => console.error(err))
}

$(".custom-list-check").change((e) => {
    console.log(e.target.dataset.customListId, e.target.checked);
    if (e.target.checked) {
        fetch(`/ajax/add_to_list.php?movie_id=<?= $movie->id ?>&list_id=${e.target.dataset.customListId}`)
            .then(res => res.json())
            .then(json => {
                if (json.success) {
                    showSnackBar(
                        `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Added to List
                        </div>`);

                } else {
                    $(e.target).val(false);
                    showSnackBar(
                        `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt add to List
                        </div>`);
                }
            });
    } else {
        fetch(`/ajax/remove_from_list.php?movie_id=<?= $movie->id ?>&list_id=${e.target.dataset.customListId}`)
            .then(res => res.json())
            .then(json => {
                if (json.success) {
                    showSnackBar(
                        `<div class="alert alert-warning" role="alert">
                            <b>Sucess</b> removed from List
                        </div>`);
                } else {
                    $(e.target).val(true);
                    showSnackBar(
                        `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt remove from List
                        </div>`);
                }
            });
    }
})
</script>

<style>
/* The snackbar - position it at the bottom*/
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    position: fixed;
    z-index: 1;
    left: 20%;
    bottom: 30px;
}

/* Show the snackbar on function call (class added with JavaScript) */
#snackbar.show {
    visibility: visible;
    /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@-webkit-keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}

@keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}
</style>