<style>
.review-content-main {
    line-height: 30px;
}
</style>

<div class="container my-5">
    <div class="my-4 w-100">
        <div class="p-3 container border bg-white">
            <div class="mb-3 p-3 bg-dark rounded">
                <a href="/movie.php?id=<?= $review->movie_id ?>" style="text-decoration: none;">
                    <span class="h2 text-white"><?= $movie->movie_name ?></span>
                    <span class="text-white">(<?= $movie->release_year ?>)</span>
                </a>
            </div>
            <div class="d-flex justify-content-between">
                <a class="btn btn-light d-flex flex-row align-items-center"
                    href="/account.php?id=<?=$review->user_id?>">
                    <img src=<?= "https://ui-avatars.com/api/?name=$review->username&rounded=true&background=888&color=fff&size=128" ?>
                        width="30" height="30" class="rounded-circle">
                    <span class="ml-3">
                        <?= $review->username ?>
                    </span>
                </a>

            </div>
            <div class="row my-3">
                <div class="col">
                    <div class="p-3">
                        <div class="d-flex flex-row align-items-center ">
                            <span class="text-secondary">Rating</span>
                            <span class="ml-2 text-primary font-weight-bold">
                                <?= $review->rating ?>
                            </span>
                        </div>
                        <hr class="my-3">
                        <div class="h5 review-content-main">
                            <?= htmlspecialchars($review->review) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row align-items-center my-1">
                <span class="flex-grow-1 d-flex flex-row align-items-center ml-3">
                    <a href="/review_vote.php?redirect=<?=$review->id?>&vote=true" class="btn">
                        <?php if($isVoted):?>
                        <span class="material-icons">
                            thumb_up
                        </span>
                        <?php else :?>
                        <span class="material-icons">
                            thumb_up_off_alt
                        </span>
                        <?php endif;?>
                    </a>
                    <span class="text-primary"><?= $review->vote_count?></span>
                    <a href="/review_vote.php?redirect=<?=$review->id?>&vote=false" class="btn">
                        <?php if(!$isVoted && $isVoted!==null):?>
                        <span class="material-icons">
                            thumb_down
                        </span>
                        <?php else :?>
                        <span class="material-icons">
                            thumb_down_off_alt
                        </span>
                        <?php endif;?>
                    </a>
                </span>
                <span class="text-secondary">
                    <small>
                        Posted On
                        <?= getDateTimeString($review->posted_at) ?>
                    </small>
                </span>
            </div>

            <?php if (isset($userReview) || $is_admin) : ?>
            <div class="mt-2 d-flex flex-row justify-content-end">

                <?php if (isset($userReview)) : ?>
                <a class="btn btn-outline-warning mx-2 d-flex flex-row align-items-center"
                    href="/review_update.php?id=<?= $review->id?>">
                    Edit Review
                    <span class="material-icons ml-2">
                        mode_edit
                    </span></a>
                <?php endif; ?>

                <a class="btn btn-outline-danger d-flex flex-row align-items-center"
                    href="/review_delete.php?review_id=<?= $review->id?>">
                    Delete Review
                    <span class="material-icons ml-2">
                        delete_outline
                    </span></a>
            </div>
            <?php else: ?>

            <div class="mt-2 d-flex flex-row justify-content-end">
                <button type="button" data-toggle="modal" data-target="#report-review-modal"
                    class="float-right btn btn-lg h5 btn-danger d-flex flex-row align-items-center">Report</button>
            </div>

            <?php endif; ?>

        </div>
    </div>

    <?php if($is_logged_in):?>

    <?php if($is_disabled):?>
    <div class="alert alert-dark">You are disabled by admin</div>
    <?php else :?>
    <div class="my-5 row">
        <form class="col-12 col-sm-10 col-md-8" action="review_comment_submit.php?review_id=<?= $review->id ?>"
            method="POST">
            <div class="bg-white border p-4">
                <div class="h4 mt-3">Add Comment</div>
                <hr class="mt-2 mb-4">
                <textarea class="form-control" placeholder="Enter Comment" cols="20" rows="3" type="text"
                    name="comment_text" value=""> </textarea>
                <div class="my-2">
                    <input type="submit" class="btn btn-primary btn btn-block" name="submit" value="Add">
                </div>
            </div>
        </form>
    </div>
    <?php endif;?>

    <?php else :?>
    <a class="btn btn-outline-dark btn-block btn-lg" href="/login.php?redirect=/review_page.php?id=<?=$review->id?>">
        Login to
        comment,
        vote</a>
    <?php endif;?>
    <div class="comments-all my-5">
        <h3>Comments</h3>
        <?php if(count($comments)):?>
        <ul class="list-group my-5">
            <?php foreach ($comments as $comment) : ?>
            <li id="comment<?=$comment->id?>" class="list-group-item">
                <div>
                    <div class="d-flex">
                        <a class="btn btn-light d-flex flex-row align-items-center"
                            href="/account.php?id=<?=$comment->user_id?>">
                            <img src=<?= "https://ui-avatars.com/api/?name=$comment->username&rounded=true&background=888&color=fff&size=128" ?>
                                width="30" height="30" class="rounded-circle">
                            <span class="ml-3">
                                <?= $comment->username ?>
                            </span>
                        </a>
                    </div>
                    <div class="p-3 my-1">
                        <div class="h5">
                            <?= (htmlspecialchars($comment->comment)) ?>
                        </div>
                    </div>
                    <div class="d-flex flex-row align-items-center justify-content-end">
                        <span class="text-secondary">
                            <small>
                                Posted On
                                <?= getDateTimeString($comment->posted_at) ?>
                            </small>
                        </span>
                    </div>

                    <?php if($comment->user_id == $user->id || $is_admin) :?>
                    <div class="mt-2 d-flex flex-row justify-content-end">
                        <a class="btn btn-outline-danger"
                            href="/review_comment_delete.php?comment_id=<?= $comment->id?>">Delete Comment</a>
                    </div>
                    <?php else: ?>
                    <div class="mt-2 d-flex flex-row justify-content-end">
                        <button type="button"
                            class="float-right btn btn-lg h5 btn-danger d-flex flex-row align-items-center"
                            onclick="open_comment_modal(<?=$comment->id?>)">Report</button>
                    </div>
                    <?php endif ?>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>

        <?php else:?>
        <h5>No comments</h5>
        <?php endif;?>
    </div>
    <div id="snackbar"></div>
    <div class="modal fade" id="report-review-modal" tabindex="-1" role="dialog"
        aria-labelledby="report-review-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="report-review-modal-label">Report Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php foreach ($report_reasons_review as $reason) { ?>
                    <input type="radio" name="review_reason" id="review_reason<?=$reason->id?>"
                        value="<?=$reason->id?>">
                    <label for="review_reason<?=$reason->id?>"><?= $reason->report_reason ?></label>
                    <br />
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" onclick="submit_report_review()">Report</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="report-comment-modal" tabindex="-1" role="dialog"
        aria-labelledby="report-comment-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="report-comment-modal-label">Report Comment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php foreach ($report_reasons_comment as $reason) { ?>
                    <input type="radio" name="comment_reason" id="comment_reason<?=$reason->id?>"
                        value="<?=$reason->id?>">
                    <label for="comment_reason<?=$reason->id?>"><?= $reason->report_reason ?></label>
                    <br />
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" onclick="submit_report_comment()">Report</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function showSnackBar(html) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
    x.innerHTML = html;
    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function open_comment_modal(id) {
    comment_id = id;
    $("#report-comment-modal").modal("show");
}

function submit_report_review() {
    let reason_id = document.querySelector('input[name="review_reason"]:checked');
    if (!reason_id)
        return;
    reason_id = reason_id.value;
    console.log(reason_id);
    $("#report-review-modal").modal("hide");
    fetch(`/ajax/report_review.php?reason_id=${reason_id}&resource_id=<?=$review->id?>`)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.success) {
                showSnackBar(
                    `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Reported review
                        </div>`);

            } else {
                showSnackBar(
                    `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt report review
                        </div>`);
            }
        }).catch(err => console.error(err))
}
var comment_id;

function submit_report_comment() {
    let id = comment_id;
    let reason_id = document.querySelector('input[name="comment_reason"]:checked');
    if (!reason_id || !id)
        return;
    reason_id = reason_id.value;
    console.log(reason_id);
    $("#report-comment-modal").modal("hide");
    fetch(`/ajax/report_comment.php?reason_id=${reason_id}&resource_id=${id}`)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.success) {
                showSnackBar(
                    `<div class="alert alert-success" role="alert">
                            <b>Sucess</b> Reported comment
                        </div>`);

            } else {
                showSnackBar(
                    `<div class="alert alert-danger" role="alert">
                            <b>Error</b> could'nt report comment
                        </div>`);
            }
        }).catch(err => console.error(err))
}

$(function() {
    var hash = window.location.hash;
    console.log(hash);
    var comment = document.querySelector(hash);
    if (comment) {
        console.log(comment);
        comment.classList.add('my-3');
        comment.classList.add('shadow-lg');
        comment.classList.add('border');
        comment.classList.add('border-danger');
    }
});
</script>

<style>
/* The snackbar - position it at the bottom*/
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    position: fixed;
    z-index: 1;
    left: 20%;
    bottom: 30px;
}

/* Show the snackbar on function call (class added with JavaScript) */
#snackbar.show {
    visibility: visible;
    /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@keyframes fadein {
    from {
        bottom: 0;
        opacity: 0;
    }

    to {
        bottom: 30px;
        opacity: 1;
    }
}

@-webkit-keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}

@keyframes fadeout {
    from {
        bottom: 30px;
        opacity: 1;
    }

    to {
        bottom: 0;
        opacity: 0;
    }
}
</style>