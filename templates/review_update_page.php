<div class="container my-5">
    <div class="review-submit">
        <form action="review_update.php?id=<?= $review->id ?>" method="POST">
            <div class="card my-2" style="width:60%;">
                <h5 class="card-header">Edit review</h5>
                <div class="card-body">
                    <div>
                        <label for="rating" class="h5 text-success">Rating</label>
                        <select class="h5" id="rating" name="rating" required>
                            <?php for ($i=1; $i < 11; $i++) :?>
                                <option value="<?=$i?>"
                                    <?php if($i == $review->rating):?>
                                        selected
                                    <?php endif; ?>
                                ><?=$i?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <textarea name="review_text" cols="50" rows="7" placeholder="Enter review"><?=$review->review?></textarea>
                    <div class="my-2">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
        <?php if(isset($success) && !$success) :?>
            <div class="alert alert-danger">
                <strong>Failure!</strong> Review not updated
            </div>
        <?php endif; ?>
    </div>
</div>