<div class="container my-5">
    <div class="mt-5">
        <ul class="nav nav-pills pt-2 pb-1 px-3 rounded">
            <li class="nav-item h5">
                <a class="nav-link h5<?=$type=="unseen"?" active":"" ?>" href="/notifications.php?type=unseen">New</a>
            </li>
            <li class="nav-item h5">
                <a class="nav-link h5<?=$type=="unseen"?"":" active" ?>" href="/notifications.php?type=seen">Read</a>
            </li>
        </ul>
    </div>
    <div class="list-group">
        <?php foreach ($notifications as $notification) { ?>
        <a href="notification_read.php?id=<?= $notification->id ?>"
            class="list-group-item list-group-item-action<?= $notification->category == 1?" list-group-item-warning":
                ($notification->category == 2 ? " list-group-item-danger":"")?>">
            <div>
                <span class="text-primary"><?= $notification->title ?></span>
                <span class="text-secondary"
                    style="float: right;"><?= getDateTimeString($notification->created_at) ?></span>
            </div>
            <div class="border my-2 p-2"><?= $notification->body ?></div>
        </a>
        <?php } ?>
    </div>
</div>