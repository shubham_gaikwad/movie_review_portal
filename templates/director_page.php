<div class="container my-5">
    <img style="height:200px;" data-director="<?=$director['director']?>" src="/images/default_profile.png">
    <h2><?=$director['director']?></h2>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse1">Directed <?= count($movies) ?>
                        movies </a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($movies as $movie) : ?>
                    <li class="list-group-item">
                        <a href="/movie.php?id=<?= $movie->movie_id ?>">
                            <span style="float: left;">
                                <img <?php if($movie->show_poster_from_imdb == 't'):?>
                                    data-imdb-id=<?= $movie->imdb_id ?> <?php endif;?> width="120" height="150"
                                    src="<?= $movie->movie_poster_name ?>" alt="<?= $movie->movie_name ?> Poster">
                            </span>
                            <span class="ml-3 h3">
                                <?= $movie->movie_name ?>
                            </span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse2">Actors worked with</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($coActors as $c) : ?>
                    <li class="list-group-item">
                        <a href="/actor.php?id=<?= $c['actor_id'] ?>">
                            <span class="h5"><?= $c['actor'] ?></span>
                        </a>
                        <a href="/ref/actor_director.php?a=<?= $c['actor_id'] ?>&d=<?= $director['id'] ?>" style=" float:
                            right;">Worked Together : <?= $c['count'] ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="list-group-item" data-toggle="collapse" href="#collapse3">Genres worked in</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <ul class="list-group">
                    <?php foreach ($genresWorkedIn as $g) : ?>
                    <li class="list-group-item">
                        <span class="h5"><?= $g['genre_name'] ?></span>
                        <a href="/ref/director_genre.php?d=<?=$director['id']?>&g=<?=$g['genre_id']?>"
                            style="float: right;">Worked In : <?= $g['count'] ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>