<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>
    <?php require_once("templates/components/head.html")?>
</head>
<body>
<?php require_once("templates/components/nav.php") ?>
<?php require_once($body); ?> 
</body>
</html>