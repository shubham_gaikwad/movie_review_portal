<div class="container my-5">
    <div class="h1 text-white p-3 p-md-4 p-lg-5 bg-dark shadow rounded mb-5">Search results</div>

    <?php 
                if(!isset($languages)) {
                    $languages = LanguageDao::getAll();
                }
                if(!isset($genres)) {
                    $genres = GenreDao::getAll();
                }
                require_once("templates/components/search_form.php");
            ?>

    <nav aria-label="..." class="mt-5 mb-3">
        <ul class="pagination">
            <li class="page-item <?php if($page == 1):?>disabled<?php endif; ?>">
                <a class="page-link"
                    href=<?= "/search.php?search=$search&genre=$genre_id&lang=$lang_id&sort=$sort&page=" . ($page - 1) ?>
                    tabindex="-1">Previous</a>
            </li>
            <li class="page-item <?php if(count($movies) < 40):?>disabled<?php endif; ?>">
                <a class="page-link"
                    href=<?= "/search.php?search=$search&genre=$genre_id&lang=$lang_id&sort=$sort&page=" . ($page + 1) ?>>Next</a>
            </li>
        </ul>
    </nav>
    <hr class="my-4">
    <?php if (count($movies) == 0) : ?>
    <h1 class="my-3">No results found</h1>
    <?php else : ?>
    <div class="row mx-auto">
        <?php foreach ($movies as $movie) : ?>
        <div class="col-6 col-sm-4 col-lg-3 my-3">
            <?php 
                            $card = new Template('templates/components/movie_card.php');
                            $card->movie_id = $movie->id;
                            $card->movie_imdb_id = $movie->imdb_id;
                            $card->movie_name = $movie->movie_name;
                            $card->movie_release_year = $movie->release_year;
                            $card->movie_poster_name = $movie->movie_poster_name;
                            $card->movie_review_count = $movie->review_count;
                            $card->movie_avg_rating = $movie->avg_rating;
                            $card->movie_show_poster_from_imdb = $movie->show_poster_from_imdb;
                            $card->renderHTML();
                        ?>
        </div>
        <?php endforeach; ?>
    </div>
    <hr class="my-4">
    <nav class="mt-2 mb-5">
        <ul class="pagination">
            <li class="page-item <?php if($page == 1):?>disabled<?php endif; ?>">
                <a class="page-link"
                    href=<?= "/search.php?search=$search&genre=$genre_id&lang=$lang_id&sort=$sort&page=" . ($page - 1) ?>
                    tabindex="-1">Previous</a>
            </li>
            <li class="page-item <?php if(count($movies) < 40):?>disabled<?php endif; ?>">
                <a class="page-link"
                    href=<?= "/search.php?search=$search&genre=$genre_id&lang=$lang_id&sort=$sort&page=" . ($page + 1) ?>>Next</a>
            </li>
        </ul>
    </nav>
    <?php endif; ?>
</div>