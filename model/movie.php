<?php


// id serial primary key,
// movie_name varchar(50) not null,
// movie_poster_name varchar(100) default 'default.jpg',
// release_year varchar(10),
// director varchar(50),
// summary text,
// avg_rating real default 0,
// review_count int default 0,
// cast_names text

class Movie
{
    public $id;
    public $movie_name;
    public $movie_poster_name;
    public $release_year;
    public $director;
    public $summary;
    public $avg_rating;
    public $review_count;
    public $cast_names;
    public $imdb_id;
    public $show_poster_from_imdb;

    public static function fromArray($arr)
    {
        $obj = new Movie();
        foreach ($arr as $key => $value) {
            $obj->$key = $value;
        }
        return $obj;
    }
}
?>