

<?php
class User
{
    public $id;
    public $username;
    public $email;
    public $rolename;
    public $profile_photo_name;

    public static function fromArray($array)
    {
        $user = new User();
        foreach ($array as $key => $value) {
            $user->$key = $value;
        }
        return $user;
    }
}
?>


