<?php

require_once ('public/include.php');

class ReportDao
{
    public static function getAllByType($type){
        $db = DbCon::getCon();
        $query = <<<Q
        select 
            wtw_reports.id, wtw_reports.user_id, wtw_reports.resource_id, 
            wtw_reports.resource_type, wtw_reports.reason_id, wtw_reports.report_status, 
            wtw_reports.reported_at, wtw_user.username, wtw_report_reason.report_reason 
        from 
            wtw_reports, wtw_user, wtw_report_reason
        where 
            wtw_reports.report_status=$1 
            and wtw_reports.user_id=wtw_user.id 
            and wtw_reports.reason_id=wtw_report_reason.id
        order by wtw_reports.reported_at
        Q;
        
        $result = pg_query_params($db, $query, [$type]);
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row;
        }
        pg_close($db);
        return $arr;
    }

    public static function ignore($report_id){
        $db = DbCon::getCon();

        $query = "update wtw_reports set report_status='IGNORED' where id=$1";
        $result = pg_query_params($db, $query, [$report_id]);
        pg_close($db);
        return $result;
    }

    public static function complete($report_id){
        $db = DbCon::getCon();

        $query = "update wtw_reports set report_status='COMPLETED' where id=$1";
        $result = pg_query_params($db, $query, [$report_id]);
        pg_close($db);
        return $result;
    }

    public static function insert($user_id, $resource_id, $resource_type, $reason_id){
        $db = DbCon::getCon();
        $query = "insert into wtw_reports(user_id, resource_id, resource_type, reason_id) values($1, $2, $3, $4)";
        $result = pg_query_params($db, $query, [$user_id, $resource_id, $resource_type, $reason_id]);
        pg_close($db);
        return $result;
    }

}
?>