

<?php
require_once('public/include.php');
class MovieDirectorDao
{
    public static $table = 'wtw_movie_director';
 
    public static function save_director($movie_id, $director_ids)
    {
        $db = DbCon::getCon();
        pg_query($db, 'BEGIN');
        pg_query_params($db, 'delete from wtw_movie_director where movie_id = $1', [$movie_id]);
        foreach ($director_ids as $a) {
            $r_a = pg_query_params($db, 
            "insert into wtw_movie_director (movie_id, director_id) values ($1, $2)", [$movie_id, $a]);
            if(!$r_a) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        pg_query('commit');
        pg_close($db);
        return [$movie_id];
    }
}
?>