<?php

require_once ('public/include.php');

class WatchlistDao
{
    public static $table = 'wtw_watchlist';


    public static function addToWatchlist($user, $movie)
    {
    
        $db = DbCon::getCon();
        $params = array($user,$movie);
        $result = pg_query_params($db,"insert into wtw_watchlist(user_id,movie_id) values($1,$2);",$params);
        pg_close($db);
        return $result;
    }

    public static function getWatchlist($user)
    {
        $db = DbCon::getCon();
        $result = pg_query_params("select * from wtw_movie wm,wtw_watchlist ww where ww.user_id=$1 and ww.movie_id=wm.id",array($user));
        $arr = array();
        while ($row = pg_fetch_assoc($result)) {
            $arr[] = Movie::fromArray($row);
        }
        pg_close($db);
        return $arr;
    
    } 
    
    public static function isInWatchlist($user,$movie)
    {
        $db = DbCon::getCon();
        $result = pg_query_params("select count(movie_id) from wtw_watchlist where user_id=$1 and movie_id=$2",array($user,$movie));
        $row=pg_fetch_row($result);
        pg_close($db);
        return $row[0];
    }

    public static function deleteFromWatchlist($user,$movie)
    {
        $db = DbCon::getCon();
        $result = pg_query_params("delete from wtw_watchlist where user_id=$1 and movie_id=$2",array($user,$movie));
        pg_close($db);
        return $result;
    }
}

?>