

<?php
require_once('public/include.php');
class ActorDao
{
    public static $table = 'wtw_actor';
 
    public static function getByMovie($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_actor, wtw_movie_actor where wtw_movie_actor.actor_id = wtw_actor.id and wtw_movie_actor.movie_id=$1",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->actor_id] = $row->actor;
        }
        pg_close($db);
        return $arr;
    }

    public static function getById($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_actor where id = $1", array($id));
        $row = pg_fetch_assoc($result, 0);
        pg_close($db);
        return $row;
    }
    
    public static function searchByName($name)
    {
        $db = DbCon::getCon();
        $name = strtolower($name);
        $result = pg_query_params($db, "select * from wtw_actor where lower(actor) like $1 || '%' order by id limit 10",array($name));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->actor;
        }
        pg_close($db);
        return $arr;
    }
    public static function insert($actor)
    {
        $db = DbCon::getCon();
        $params = array($actor);
        $result = pg_query_params($db,"insert into wtw_actor(actor) values($1);",$params);
        pg_close($db);
        return $result;
    }
}
?>