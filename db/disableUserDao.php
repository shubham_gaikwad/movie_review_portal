<?php
require_once('public/include.php');

class DisableUserDao
{
    public static function disable($id, $reason)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, 
            "insert into wtw_disable_user(user_id, reason) values ($1, $2)",
            array($id, $reason)
        );
        pg_close($db);
        return $result;
    }

    public static function enable($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, 
            "delete from wtw_disable_user where user_id = $1",
            array($id)
        );
        pg_close($db);
        return $result;
    }

    public static function getUser($id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_disable_user where user_id=$1", array($id));
        $user = pg_fetch_object($result);
        pg_close($db);
        return $user;
    }

    public static function getAll() {
        $db = DbCon::getCon();
        $result = pg_query($db, "select user_id, reason, username, disabled_at from wtw_disable_user wd, wtw_user where wtw_user.id=wd.user_id");
       
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            array_push($arr, $row);
        }
        pg_close($db);
        return $arr;
    }

}
?>