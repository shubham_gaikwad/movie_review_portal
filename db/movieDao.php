<?php
require_once('public/include.php');
class MovieDao
{
    public static $table = 'wtw_movie';
    // public static function createUser(User $user, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $array = (array) $user;
    //     $array['pass'] = $pass;
    //     $result = pg_insert($db, UserDao::$table, $array);
    //     pg_close($db);
    //     return $result;
    // }
    // public static function authenticateUser($username, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $result = pg_query_params($db, "select * from wtw_user where username = $1", array($username));
    //     $user = pg_fetch_assoc($result, 0);
    //     $result = null;
    //     if ($user['pass'] === $pass)
    //         $result = User::fromArray($user);
    //     pg_close($db);
    //     return $result;
    // }
    public static function test()
    {
        $db = DbCon::getCon();
        $result = pg_query($db, "select * from wtw_movie, wtw_movie_language where wtw_movie_language.movie_id = wtw_movie.id and language_id = 63 order by release_year desc, wtw_movie.id limit 20");
        $arr1 = array();
        while ($row = pg_fetch_assoc($result)) {
            $arr1[] = Movie::fromArray($row);
        }
        $result = pg_query($db, "select * from wtw_movie, wtw_movie_language where wtw_movie_language.movie_id = wtw_movie.id and language_id = 93 order by release_year desc, wtw_movie.id limit 20");
        $arr2 = array();
        while ($row = pg_fetch_assoc($result)) {
            $arr2[] = Movie::fromArray($row);
        }
        $result = pg_query($db, "select * from wtw_movie, wtw_movie_language where wtw_movie_language.movie_id = wtw_movie.id and language_id = 147 order by release_year desc, wtw_movie.id limit 20");
        $arr3 = array();
        while ($row = pg_fetch_assoc($result)) {
            $arr3[] = Movie::fromArray($row);
        }
        pg_close($db);
        return [$arr1, $arr2, $arr3];
    }

    public static function getById($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_movie where id = $1", array($id));
        $row = pg_fetch_assoc($result, 0);
        pg_close($db);
        if ($row) {
            return Movie::fromArray($row);
        }
        return null;
    }
    public static function getByActor($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_movie, wtw_movie_actor where wtw_movie_actor.movie_id = wtw_movie.id and wtw_movie_actor.actor_id=$1 order by release_year desc",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->movie_id] = $row;
        }
        pg_close($db);
        return $arr;
    }
    public static function getByDirector($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_movie, wtw_movie_director where wtw_movie_director.movie_id = wtw_movie.id and wtw_movie_director.director_id=$1 order by release_year desc",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->movie_id] = $row;
        }
        pg_close($db);
        return $arr;
    }
    public static function search($search, $genre_id, $lang_id, $sort, $page)
    {

        $limit = 40;
        $offset = ($page - 1) * $limit;
        $db = DbCon::getCon();
        $result = null;

        $query = "select distinct m.id from wtw_movie as m, wtw_movie_genre as mg, wtw_movie_language as ml where mg.movie_id = m.id and ml.movie_id = m.id and lower(movie_name) like '%' || $1 || '%' ";
        if ($genre_id != '') {
            $query .= " and mg.genre_id = $2 ";
        } else {
            $query .= " and mg.genre_id > $2 ";
        }

        if ($lang_id != '') {
            $query .= " and ml.language_id = $3 ";
        } else {
            $query .= " and ml.language_id > $3 ";
        }

        $query = "select * from wtw_movie where id in ($query)";
        $sort_q = array(" order by movie_name asc ", " order by movie_name desc ", " order by release_year asc ", " order by release_year desc ");
        $query .= $sort_q[$sort - 1] . ", wtw_movie.id limit $limit offset $offset";
        $result = pg_query_params($db, $query, array(strtolower($search), $genre_id == '' ? 0 : $genre_id, $lang_id == '' ? 0 : $lang_id));

        // if ($genre_id != '') {
        //     $query = "select * from wtw_movie where id in (select movie_id from wtw_movie_genre where genre_id = $1) and movie_name like '%' || $2 || '%' order by movie_name limit $limit offset $offset";
        //     $result = pg_query_params($db, $query, array($genre_id, $search));
        // } else {
        //     $query = "select * from wtw_movie where lower(movie_name) like '%' || $1 || '%' order by movie_name limit $limit offset $offset";
        //     $result = pg_query_params($db, $query, array(strtolower($search)));
        // }

        if (!$result) {
            echo pg_last_error($db);
            pg_close($db);
            return array();
        }

        $rows = pg_fetch_all($result);
        if (!$rows) {
            echo pg_last_error($db);
            pg_close($db);
            return array();
        }
        pg_close($db);
        return array_map((function ($r) {
            return Movie::fromArray($r);
        }), $rows);
    }

    public static function insert($name, $year, $summary, $languages, $genres)
    {
        $db = DbCon::getCon();
        $params = array($name, $year, $summary);
        pg_query($db, 'BEGIN');
        $result = pg_query_params($db,"insert into wtw_movie(movie_name, release_year, summary) values($1, $2, $3)",$params);
        if(!$result) {
            pg_query('rollback');
            pg_close($db);
            return false;
        }
        $r_id = pg_query($db, "select currval('wtw_movie_id_seq')");
        $id = pg_fetch_row($r_id)[0];
        if(!$r_id || !isset($id)) {
            pg_query('rollback');
            pg_close($db);
            return false;
        }
        foreach ($genres as $g) {
            $r_g = pg_query_params($db, 
            "insert into wtw_movie_genre (movie_id, genre_id) values ($1, $2)", [$id, $g]);
            if(!$r_g) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        foreach ($languages as $l) {
            $r_l = pg_query_params($db, 
            "insert into wtw_movie_language (movie_id, language_id) values ($1, $2)", [$id, $l]);
            if(!$r_l) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        pg_query('commit');
        pg_close($db);
        return [$id];
    }
    public static function deleteById($id) {
        $db = DbCon::getCon();
        $result = pg_query($db, "delete from wtw_movie where id=$id");
        pg_close($db);
        return $result;
    }

    public static function update($id, $name, $year, $summary, $languages, $genres)
    {
        $db = DbCon::getCon();
        $params = array($name, $year, $summary, $id);
        pg_query($db, 'BEGIN');
        $result = pg_query_params($db,"update wtw_movie set movie_name=$1, release_year=$2, summary=$3 where id = $4",$params);
        if(!$result) {
            pg_query('rollback');
            pg_close($db);
            return false;
        }
        pg_query_params($db, 'delete from wtw_movie_genre where movie_id = $1', [$id]);
        foreach ($genres as $g) {
            $r_g = pg_query_params($db, 
            "insert into wtw_movie_genre (movie_id, genre_id) values ($1, $2)", [$id, $g]);
            if(!$r_g) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        pg_query_params($db, 'delete from wtw_movie_language where movie_id = $1', [$id]);
        foreach ($languages as $l) {
            $r_l = pg_query_params($db, 
            "insert into wtw_movie_language (movie_id, language_id) values ($1, $2)", [$id, $l]);
            if(!$r_l) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        pg_query('commit');
        pg_close($db);
        return [$id];
    }
    
    public static function searchByName($name)
    {
        $db = DbCon::getCon();
        $name = strtolower($name);
        $result = pg_query_params($db, "select * from wtw_movie where lower(movie_name) like $1 || '%' order by id limit 10",array($name));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->movie_name;
        }
        pg_close($db);
        return $arr;
    }

    public static function setMoviePosterByIMDB($id, $imdbId)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"update wtw_movie set imdb_id=$1, movie_poster_name='/images/default_poster.svg', show_poster_from_imdb = 'true' where id = $2", [$imdbId, $id]);
        pg_close($db);
        return $result;
    }

    public static function setMoviePosterByFILE($id, $filename)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"update wtw_movie set movie_poster_name=$1, show_poster_from_imdb = 'false' where id = $2", [$filename, $id]);
        pg_close($db);
        return $result;
    }
}

//select * from wtw_movie where id in (select movie_id from wtw_movie_genre where genre_id = 1) and movie_name like '%a%' order by movie_name limit 30 offset 0
?>