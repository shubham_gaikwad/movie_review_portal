

<?php
require_once('public/include.php');
class GenreDao
{
    public static $table = 'wtw_genre';
    // public static function createUser(User $user, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $array = (array) $user;
    //     $array['pass'] = $pass;
    //     $result = pg_insert($db, UserDao::$table, $array);
    //     pg_close($db);
    //     return $result;
    // }
    // public static function authenticateUser($username, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $result = pg_query_params($db, "select * from wtw_user where username = $1", array($username));
    //     $user = pg_fetch_assoc($result, 0);
    //     $result = null;
    //     if ($user['pass'] === $pass)
    //         $result = User::fromArray($user);
    //     pg_close($db);
    //     return $result;
    // }
    public static function getAll()
    {
        $db = DbCon::getCon();
        $result = pg_query($db, "select * from wtw_genre");
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->genre_name;
        }
        pg_close($db);
        return $arr;
    }

    public static function getByMovie($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_genre,wtw_movie_genre where movie_id = wtw_movie_genre.movie_id and wtw_genre.id=wtw_movie_genre.genre_id and movie_id=$1;",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->genre_id] = $row->genre_name;
        }
        pg_close($db);
        return $arr;
    }
    
    public static function insert($genre)
    {
        $db = DbCon::getCon();
        $params = array($genre);
        $result = pg_query_params($db,"insert into wtw_genre(genre_name) values($1);",$params);
        pg_close($db);
        return $result;
    }
}
?>