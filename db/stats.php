<?php
require_once('public/include.php');
class Stats
{

    public static function getCoStarsOfActorWithCount($actor_id) {
        $query = <<<Q
        select actor_id, count(actor_id), actor 
            from wtw_movie_actor, wtw_actor 
            where movie_id in (select movie_id from wtw_movie_actor where actor_id = $1) 
                and 
                wtw_movie_actor.actor_id = wtw_actor.id  
            group by actor_id, actor 
            having actor_id <> $1
            order by count desc
            limit 50;
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$actor_id]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return $arr;
    } 

    public static function getDirectorsOfActorWithCount($actor_id) {
        $query = <<<Q
        select director_id, count(director_id), director 
            from wtw_movie_director, wtw_director 
            where movie_id in (select movie_id from wtw_movie_actor where actor_id = $1) 
                and 
                wtw_movie_director.director_id = wtw_director.id  
            group by director_id, director 
            order by count desc
            limit 50;
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$actor_id]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return $arr;
    } 
    
    public static function getGenresOfActorWithCount($actor_id) {
        $query = <<<Q
        select genre_id, count(genre_id), genre_name 
            from wtw_movie_genre, wtw_genre 
            where movie_id in (select movie_id from wtw_movie_actor where actor_id = $1) 
                and 
                wtw_movie_genre.genre_id = wtw_genre.id  
            group by genre_id, genre_name
            order by count desc
            limit 10;
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$actor_id]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return $arr;
    } 
 
    public static function getActorsOfDirectorWithCount($director_id) {
        $query = <<<Q
        select actor_id, count(actor_id), actor 
            from wtw_movie_actor, wtw_actor 
            where movie_id in (select movie_id from wtw_movie_director where director_id = $1) 
                and 
                wtw_movie_actor.actor_id = wtw_actor.id  
            group by actor_id, actor 
            order by count desc
            limit 50;
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$director_id]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return $arr;
    }

    public static function getGenresOfDirectorWithCount($director_id) {
        $query = <<<Q
        select genre_id, count(genre_id), genre_name 
            from wtw_movie_genre, wtw_genre 
            where movie_id in (select movie_id from wtw_movie_director where director_id = $1) 
                and 
                wtw_movie_genre.genre_id = wtw_genre.id  
            group by genre_id, genre_name
            order by count desc
            limit 10;
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$director_id]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return $arr;
    }
    
    /**
     * @return Movie[]
     */
    public static function getMoviesRefActorActor($id1, $id2) {
        $query = <<<Q
        select * from wtw_movie where id in (
            select movie_id from wtw_movie_actor where actor_id = $1 
            intersect
            select movie_id from wtw_movie_actor where actor_id = $2
        ) order by release_year desc
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$id1, $id2]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return array_map((function ($r) {
            return Movie::fromArray($r);
        }), $arr);
    }

    /**
     * @return Movie[]
     */
    public static function getMoviesRefActorDirector($id1, $id2) {
        $query = <<<Q
        select * from wtw_movie where id in (
            select movie_id from wtw_movie_actor where actor_id = $1 
            intersect
            select movie_id from wtw_movie_director where director_id = $2
        ) order by release_year desc
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$id1, $id2]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return array_map((function ($r) {
            return Movie::fromArray($r);
        }), $arr);
    }

    /**
     * @return Movie[]
     */
    public static function getMoviesRefActorGenre($id1, $id2) {
        $query = <<<Q
        select * from wtw_movie where id in (
            select movie_id from wtw_movie_actor where actor_id = $1 
            intersect
            select movie_id from wtw_movie_genre where genre_id = $2
        ) order by release_year desc
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$id1, $id2]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return array_map((function ($r) {
            return Movie::fromArray($r);
        }), $arr);
    }

    /**
     * @return Movie[]
     */
    public static function getMoviesRefDirectorGenre($id1, $id2) {
        $query = <<<Q
        select * from wtw_movie where id in (
            select movie_id from wtw_movie_director where director_id = $1 
            intersect
            select movie_id from wtw_movie_genre where genre_id = $2
        ) order by release_year desc
        Q;
        $db = DbCon::getCon();
        $result = pg_query_params($db, $query, [$id1, $id2]);
        $arr = pg_fetch_all($result);
        pg_close($db);
        return array_map((function ($r) {
            return Movie::fromArray($r);
        }), $arr);
    }
}
?>