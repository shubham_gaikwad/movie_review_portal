<?php

require_once ('public/include.php');

class ReportReasonDao
{
    public static function getAllReasonsByType($type){
        $db = DbCon::getCon();
        $query = <<<Q
        select id, report_reason
        from wtw_report_reason
        where resource_type = $1
        Q;
        $result = pg_query_params($db, $query, [$type]);
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row;
        }
        pg_close($db);
        return $arr;
    }
}
?>