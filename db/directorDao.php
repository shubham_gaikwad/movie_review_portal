

<?php
require_once('public/include.php');
class DirectorDao
{
    public static $table = 'wtw_director';
 
    public static function getByMovie($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_director, wtw_movie_director where wtw_movie_director.director_id = wtw_director.id and wtw_movie_director.movie_id=$1",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->director_id] = $row->director;
        }
        pg_close($db);
        return $arr;
    }

    public static function getById($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_director where id = $1", array($id));
        $row = pg_fetch_assoc($result, 0);
        pg_close($db);
        return $row;
    }

    public static function searchByName($name)
    {
        $db = DbCon::getCon();
        $name = strtolower($name);
        $result = pg_query_params($db, "select * from wtw_director where lower(director) like $1 || '%' order by id limit 10",array($name));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->director;
        }
        pg_close($db);
        return $arr;
    }

    public static function insert($director)
    {
        $db = DbCon::getCon();
        $params = array($director);
        $result = pg_query_params($db,"insert into wtw_director(director) values($1);",$params);
        pg_close($db);
        return $result;
    }
}
?>