<?php
require_once('public/include.php');

class FollowDao
{
    public static $table = 'wtw_follow';

    public static function follow_user($follower_id,$following_id)
    {
        $db = DbCon::getCon();
        $query = "insert into wtw_follow values($1,$2)";
        $result = pg_query_params($query,array($follower_id,$following_id));
        pg_close($db);
        return $result;
    }

    public static function unfollow_user($follower_id,$following_id)
    {
        $db = DbCon::getCon();
        $query = "delete from wtw_follow where user_id=$1 and following_id=$2";
        $result = pg_query_params($query,array($follower_id,$following_id));
        pg_close($db);
        return $result;
    }

    public static function getFollowings($user_id)
    {
        $db = DbCon::getCon();
        $query = "select id,username,profile_photo_name from wtw_user where id in
                (select following_id from wtw_follow where user_id=$1)";
        $result = pg_query_params($query,array($user_id));
        $followings = array();
        
        while ($row = pg_fetch_object($result)) {
            $following = User::fromArray($row);
            array_push($followings,$following);
        }
        pg_close($db);
        
        return $followings;
    }

    public static function getFollowers($user_id)
    {
        $db = DbCon::getCon();
        $query = "select id,username,profile_photo_name from wtw_user where id in
        (select user_id from wtw_follow where following_id=$1)";
        $result = pg_query_params($query,array($user_id));
        $followers = array();
        
        while ($row = pg_fetch_object($result)) {
            $follower = User::fromArray($row);
            array_push($followers,$follower);
        }
        pg_close($db);
        
        return $followers;
    }


    public static function countFollowers($user_id)
    {
        $db = DbCon::getCon();
        $query = "select count(*) from wtw_follow where following_id=$1";
        $result = pg_query_params($query,array($user_id));
        
        $row=pg_fetch_row($result);
        
        pg_close($db);
        
        return $row[0];
    }

    public static function countFollowing($user_id)
    {
        $db = DbCon::getCon();
        $query = "select count(*) from wtw_follow where user_id=$1";
        $result = pg_query_params($query,array($user_id));
        
        $row=pg_fetch_row($result);
        
        pg_close($db);
        
        return $row[0];
    }

    public static function is_following($user_id,$following_id)
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_follow where user_id=$1 and following_id=$2";
        $result = pg_query_params($query,array($user_id,$following_id));
        pg_close($db);
        return pg_num_rows($result)? true : false;
    }
}

?>