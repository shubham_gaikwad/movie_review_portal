

<?php
require_once('public/include.php');
class MovieActorDao
{
    public static $table = 'wtw_movie_actor';
 
    public static function save_cast($movie_id, $actor_ids)
    {
        $db = DbCon::getCon();
        pg_query($db, 'BEGIN');
        pg_query_params($db, 'delete from wtw_movie_actor where movie_id = $1', [$movie_id]);
        foreach ($actor_ids as $a) {
            $r_a = pg_query_params($db, 
            "insert into wtw_movie_actor (movie_id, actor_id) values ($1, $2)", [$movie_id, $a]);
            if(!$r_a) {
                pg_query('rollback');
                pg_close($db);
                return false;
            }
        }
        pg_query('commit');
        pg_close($db);
        return [$movie_id];
    }
}
?>