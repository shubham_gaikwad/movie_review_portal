<?php
require_once('public/include.php');
class UserDao
{
    public static $table = 'wtw_user';
    public static function createUser(User $user, $pass)
    {
        $db = DbCon::getCon();
        $array = (array) $user;
        $array['pass'] = $pass;
        $result = pg_insert($db, UserDao::$table, $array);
        pg_close($db);
        return $result;
    }
    public static function authenticateUser($username, $pass)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_user where username = $1", array($username));
        $user = pg_fetch_assoc($result, 0);
        $result = null;
        if (password_verify($pass, $user['pass']))
            $result = User::fromArray($user);
        pg_close($db);
        return $result;
    }

    public static function getUser($id){
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_user where id=$1", array($id));
        $user = pg_fetch_assoc($result, 0);
        $result = User::fromArray($user);
        pg_close($db);
        return $result;
    }

    public static function searchByName($name)
    {
        $db = DbCon::getCon();
        $name = strtolower($name);
        $result = pg_query_params($db, "select * from wtw_user where lower(username) like $1 || '%' order by id limit 10",array($name));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->username;
        }
        pg_close($db);
        return $arr;
    }
}
?>