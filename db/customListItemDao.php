<?php
require_once('public/include.php');
class CustomListItemDao
{
    public static $table = 'wtw_custom_list_item';

    public static function getMovieItemsByListId($list_id)
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_custom_list_item, wtw_movie where wtw_movie.id = wtw_custom_list_item.movie_id and custom_list_id = $1";
        $result = pg_query_params($db, $query, [$list_id]);
        $rows = pg_fetch_all($result);
        pg_close($db);
        return $rows;
    }
    public static function getListsByUserHavingMovie($user_id, $movie_id)
    {
        $db = DbCon::getCon();
        $query = <<<Q
            select custom_list_id from 
                wtw_custom_list_item, wtw_custom_list 
                where 
                    wtw_custom_list_item.custom_list_id = wtw_custom_list.id  
                    and
                    user_id = $1
                    and
                    movie_id = $2
        Q;
        $result = pg_query_params($db, $query, [$user_id, $movie_id]);
        $rows = pg_fetch_all($result);
        pg_close($db);
        return array_map(function ($r) {
            return $r['custom_list_id'];
        }, $rows);
    }
    // public static function getById($id)
    // {
    //     $db = DbCon::getCon();
    //     $query = "select * from wtw_custom_list where id = $1";
    //     $result = pg_query_params($db, $query, array($id));
    //     $rows = pg_fetch_assoc($result);
    //     pg_close($db);
    //     return $rows;
    // }
    // public static function searchByName($name)
    // {
    //     $db = DbCon::getCon();
    //     $name = strtolower($name);
    //     $result = pg_query_params($db, "select * from wtw_actor where lower(actor) like $1 || '%' order by id limit 10",array($name));
    //     $arr = array();
    //     while ($row = pg_fetch_object($result)) {
    //         $arr[$row->id] = $row->actor;
    //     }
    //     pg_close($db);
    //     return $arr;
    // }
    public static function insert($list_id, $movie_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"insert into wtw_custom_list_item(custom_list_id, movie_id) values($1, $2);", [$list_id, $movie_id]);
        pg_close($db);
        return $result;
    }

    public static function delete($list_id, $movie_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"delete from wtw_custom_list_item where custom_list_id = $1 and movie_id = $2", [$list_id, $movie_id]);
        pg_close($db);
        return $result;
    }
}
?>