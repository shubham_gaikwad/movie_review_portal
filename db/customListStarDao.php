<?php
require_once('public/include.php');
class CustomListStarDao
{
    public static $table = 'wtw_custom_list_star';

    public static function insert($list_id, $user_id)
    {
        // print_r($list_id);
        // print_r($user_id);
        $db = DbCon::getCon();
        $result = pg_query_params($db,"insert into wtw_custom_list_star(custom_list_id, user_id) values($1, $2);", [$list_id, $user_id]);
        // print_r($result);
        pg_close($db);
        return $result;
    }

    public static function delete($list_id, $user_id)
    {
        // print_r($list_id);
        // print_r($user_id);
        $db = DbCon::getCon();
        $result = pg_query_params($db,"delete from wtw_custom_list_star where custom_list_id = $1 and user_id = $2", [$list_id, $user_id]);
        // print_r($result);
        pg_close($db);
        return $result;
    }

    public static function is_stared($list_id, $user_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"select * from wtw_custom_list_star where custom_list_id = $1 and user_id = $2", [$list_id, $user_id]);
        $result = pg_fetch_all($result);
        pg_close($db);
        return $result;
    }
}
?>