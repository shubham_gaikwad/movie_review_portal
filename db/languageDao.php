

<?php
require_once('public/include.php');
class LanguageDao
{
    public static $table = 'wtw_language';
    // public static function createUser(User $user, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $array = (array) $user;
    //     $array['pass'] = $pass;
    //     $result = pg_insert($db, UserDao::$table, $array);
    //     pg_close($db);
    //     return $result;
    // }
    // public static function authenticateUser($username, $pass)
    // {
    //     $db = DbCon::getCon();
    //     $result = pg_query_params($db, "select * from wtw_user where username = $1", array($username));
    //     $user = pg_fetch_assoc($result, 0);
    //     $result = null;
    //     if ($user['pass'] === $pass)
    //         $result = User::fromArray($user);
    //     pg_close($db);
    //     return $result;
    // }
    public static function getAll()
    {
        $db = DbCon::getCon();
        $result = pg_query($db, "select * from wtw_language");
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row->language_name;
        }
        pg_close($db);
        return $arr;
    }

    public static function getByMovie($id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select * from wtw_language,wtw_movie_language where movie_id = wtw_movie_language.movie_id and wtw_language.id=wtw_movie_language.language_id and movie_id=$1;",array($id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->language_id] = $row->language_name;
        }
        pg_close($db);
        return $arr;
    }

    public static function insert($language)
    {
        $db = DbCon::getCon();
        $params = array($language);
        $result = pg_query_params($db,"insert into wtw_language(language_name) values($1);",$params);
        pg_close($db);
        return $result;
    }
}
?>