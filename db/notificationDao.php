<?php
require_once('public/include.php');

class NotificationDao
{
    public static $table = 'wtw_notifications';

    public static function readNotification($id){
        $db = DbCon::getCon();
        $query = "update wtw_notifications set unread=false where id=$1";
        $result = pg_query_params($query, array($id));
        pg_close($db);
        return $result;
    }

    public static function getNotificationsByUser($user_id, $type = "all")
    {
        $db = DbCon::getCon();
        if($type == "all") {
            $query = "select * from wtw_notifications where recipient_id=$1 order by created_at desc";
        } elseif($type == "seen") {
            $query = "select * from wtw_notifications where recipient_id=$1 and unread=false order by created_at desc";
        } else {
            $query = "select * from wtw_notifications where recipient_id=$1 and unread=true order by created_at desc";
        }

        $result = pg_query_params($query, array($user_id));
        $notifications = array();
        while ($row = pg_fetch_object($result)) 
        {
            array_push($notifications, $row);
        }
        pg_close($db);
        return $notifications;
    }

    public static function getNotificationById($id)
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_notifications where id=$1";
        $result = pg_query_params($query, array($id));
        $row = pg_fetch_object($result);
        pg_close($db);
        return $row;
    }

    public static function warnUser($id, $reason){
        $db = DbCon::getCon();
        $query = "insert into wtw_notifications (recipient_id, title, body, category) values ($1, $2, $3, 1)";
        $result = pg_query_params($query, array($id, "Warning", $reason));
        pg_close($db);
        return $result;
    }
}
?>