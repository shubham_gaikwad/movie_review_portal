<?php
require_once('public/include.php');

class voteDao
{
    public static $table = 'wtw_review_vote';

    public static function registerVote($user,$review,$vote)
    {

        $db = DbCon::getCon();
        $result = pg_query_params("select register_vote($1,$2,$3)",array($user,$review,$vote));
        pg_close($db);
        return $result;
    }

    public static function voteIs($user,$review)
    {
        $db = DbCon::getCon();
        $result = pg_query_params("select isupvote from wtw_review_vote where user_id=$1 and review_id=$2",array($user,$review));
        $row = pg_fetch_row($result);
        pg_close($db);
        return $row[0]=='t'?1:($row[0]=='f'?0:null);
    }
    
}


?>