<?php
require_once('public/include.php');
class CustomListDao
{
    public static $table = 'wtw_custom_list';

    public static function getByUserId($user_id, $only_public = true)
    {
        $db = DbCon::getCon();
        if($only_public) {
            $query = "select * from wtw_custom_list where user_id = $1 and is_public = 't'";
        } else {
            $query = "select * from wtw_custom_list where user_id = $1";
        }
        $result = pg_query_params($db, $query, array($user_id));
        $rows = pg_fetch_all($result);
        pg_close($db);
        return $rows;
    }

    public static function getTopLists()
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_custom_list where is_public = 't' order by star_count desc, movie_count desc";
        $result = pg_query($db, $query);
        $rows = pg_fetch_all($result);
        pg_close($db);
        return $rows;
    }
    
    public static function getStarredByUserId($user_id)
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_custom_list where id in (select custom_list_id from wtw_custom_list_star where user_id = $1) and is_public = 't' order by star_count desc, movie_count desc";
        $result = pg_query_params($db, $query, array($user_id));
        $rows = pg_fetch_all($result);
        pg_close($db);
        return $rows;
    }

    public static function getById($id)
    {
        $db = DbCon::getCon();
        $query = "select * from wtw_custom_list where id = $1";
        $result = pg_query_params($db, $query, array($id));
        $rows = pg_fetch_assoc($result);
        pg_close($db);
        return $rows;
    }
    
    public static function insert($user_id, $list_name, $is_private)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"insert into wtw_custom_list(user_id, name, is_public) values($1, $2, $3);", [$user_id, $list_name, $is_private? 'f' : 't']);
        pg_close($db);
        return $result;
    }
    
    public static function delete($list_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"delete from wtw_custom_list where id=$1", [$list_id]);
        pg_close($db);
        return $result;
    }

    public static function make_public($list_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"update wtw_custom_list set is_public = 't' where id=$1", [$list_id]);
        pg_close($db);
        return $result;
    }

    public static function make_private($list_id)
    {
        $db = DbCon::getCon();
        $result = pg_query_params($db,"update wtw_custom_list set is_public = 'f' where id=$1", [$list_id]);
        pg_close($db);
        return $result;
    }
}
?>