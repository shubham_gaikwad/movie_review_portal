<?php

require_once ('public/include.php');

class ReviewDao
{
    public static $table = 'wtw_review';

    public static function insertReview($user_id, $movie_id, $review, $rating)
    {
        $db = DbCon::getCon();
        $insert=pg_query_params($db, "insert into wtw_review(user_id, movie_id, review, rating) values($1, $2, $3, $4)", [$user_id, $movie_id, $review, $rating]);
        pg_close($db);

        return !!$insert;
    }
    public static function updateReview($id, $review, $rating)
    {
        $db = DbCon::getCon();
        $insert=pg_query_params($db, "update wtw_review set review = $1, rating = $2 where id = $3", [$review, $rating, $id]);
        pg_close($db);

        return !!$insert;
    }
    public static function getAllReviews($movie_id, $sortBy) {
        $db = DbCon::getCon();
        $query = "select wtw_review.id, posted_at, vote_count, user_id, rating, review, wtw_user.username from wtw_review, wtw_user where movie_id=$1 and wtw_review.user_id=wtw_user.id ";
        
        if($sortBy === "recent") {
            $query .= "order by posted_at desc";    
        } else {
            $query .= "order by vote_count desc";        
        }
        $result = pg_query_params($db, $query, [$movie_id]);
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row;
        }
        pg_close($db);
        return $arr;
    }

    public static function getAllReviewsByUser($user_id) {
        $db = DbCon::getCon();
        $query = "select wr.id,movie_id, movie_name, movie_poster_name, show_poster_from_imdb, imdb_id, release_year, review, vote_count, posted_at, rating
                    from wtw_movie wm, wtw_review wr where wm.id=wr.movie_id and wr.user_id=$1    
                ";
        $result = pg_query_params($db, $query, array($user_id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            array_push($arr, $row);
        }
        pg_close($db);
        return $arr;
    }
    
    public static function getFeed($user_id) {
        $db = DbCon::getCon();
        $query = <<<Q
        select 
            wtw_review.id, wtw_review.posted_at, wtw_review.vote_count, wtw_review.user_id, 
            wtw_review.rating, wtw_review.review, wtw_review.movie_id, 
            wtw_user.username,
            wtw_movie.movie_name, wtw_movie.release_year  
        from 
            wtw_review, wtw_user, wtw_movie
        where 
            wtw_review.movie_id = wtw_movie.id
        and    
            wtw_review.user_id = wtw_user.id
        and
            wtw_review.id in
                ((select id from wtw_review where user_id in (select following_id from wtw_follow where user_id = $1))
                union
                (select id from wtw_review where movie_id in (select movie_id from wtw_watchlist where user_id = $1)))
        order by wtw_review.posted_at desc
        limit 10
        Q;
        $result = pg_query_params($db, $query, array($user_id));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            array_push($arr, $row);
        }
        pg_close($db);
        return $arr;
    }

    public static function getFeedNext($user_id, $last_posted_at) {
        $db = DbCon::getCon();
        $query = <<<Q
        select 
            wtw_review.id, wtw_review.posted_at, wtw_review.vote_count, wtw_review.user_id, 
            wtw_review.rating, wtw_review.review, wtw_review.movie_id, 
            wtw_user.username,
            wtw_movie.movie_name, wtw_movie.release_year  
        from 
            wtw_review, wtw_user, wtw_movie
        where 
            wtw_review.movie_id = wtw_movie.id
        and    
            wtw_review.user_id = wtw_user.id
        and
            wtw_review.posted_at < $2
        and
            wtw_review.id in
                ((select id from wtw_review where user_id in (select following_id from wtw_follow where user_id = $1))
                union
                (select id from wtw_review where movie_id in (select movie_id from wtw_watchlist where user_id = $1)))
        order by wtw_review.posted_at desc
        limit 10
        Q;
        $result = pg_query_params($db, $query, array($user_id, $last_posted_at));
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            array_push($arr, $row);
        }
        pg_close($db);
        return $arr;
    }

    public static function getByID($review_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select wtw_review.id, posted_at, vote_count, user_id, rating, review, wtw_user.username, movie_id from wtw_review, wtw_user where wtw_review.id=$1 and wtw_review.user_id=wtw_user.id", [$review_id]);
        $row = pg_fetch_object($result);
        pg_close($db);
        return $row;
    }

    public static function deleteByID($review_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "delete from wtw_review where id=$1", [$review_id]);
        pg_close($db);
        return $result;
    }

}
?>