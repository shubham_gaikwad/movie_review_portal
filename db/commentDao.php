<?php

require_once ('public/include.php');

class CommentDao
{
    public static $table = 'wtw_comment';

    public static function insertComment($user_id, $review_id, $comment)
    {
        $db = DbCon::getCon();
        $insert=pg_query_params($db, "insert into wtw_comment(user_id, comment, review_id) values($1,$2,$3)", [$user_id, $comment, $review_id]);
        pg_close($db);

        return !!$insert;
    }

    public static function getAllComments($review_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select wtw_comment.id, wtw_comment.posted_at, user_id, comment, wtw_user.username from wtw_comment, wtw_user where review_id=$1 and wtw_comment.user_id=wtw_user.id order by wtw_comment.posted_at asc", [$review_id]);
        $arr = array();
        while ($row = pg_fetch_object($result)) {
            $arr[$row->id] = $row;
        }
        pg_close($db);
        return $arr;
    }

    public static function getByID($comment_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select wtw_comment.id, user_id, comment, review_id, wtw_user.username from wtw_comment, wtw_user where wtw_comment.id=$1 and wtw_comment.user_id=wtw_user.id", [$comment_id]);
        $row = pg_fetch_object($result);
        pg_close($db);
        return $row;
    }

    public static function deleteByID($comment_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "delete from wtw_comment where id=$1", [$comment_id]);
        pg_close($db);
        return $result;
    }

    public static function getReviewId($comment_id) {
        $db = DbCon::getCon();
        $result = pg_query_params($db, "select review_id from wtw_comment where id=$1", [$comment_id]);
        $result = pg_fetch_row($result)[0];
        pg_close($db);
        return $result;
    }

}


?>