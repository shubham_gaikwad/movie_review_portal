create table wtw_user (
    id serial primary key,
    username varchar(20) unique not null,
    email varchar(50) unique not null,
    pass varchar(128) not null,
    rolename varchar(20) not null check(rolename in ('Admin', 'General')),
    profile_photo_name varchar(100) default 'default.png'
);

create table wtw_language (
    id serial primary key,
    language_name varchar(100) not null unique
);


create table wtw_genre (
    id serial primary key,
    genre_name varchar(20) not null unique
);

create table wtw_movie (
    id serial primary key,
    movie_name varchar(200) not null,
    movie_poster_name varchar(100) default 'default.jpg',
    release_year varchar(100),
    director varchar(100),
    summary text,
    avg_rating real default 0,
    review_count int default 0,
    cast_names text
);

create table wtw_movie_genre (
    id serial primary key,
    movie_id int references wtw_movie(id),
    genre_id int references wtw_genre(id),
    unique(movie_id, genre_id)
);

create table wtw_movie_language (
    id serial primary key,
    movie_id int references wtw_movie(id),
    language_id int references wtw_language(id),
    unique(movie_id, language_id)
);

create table wtw_review (
    id serial primary key,
    user_id int references wtw_user(id),
    movie_id int references wtw_movie(id),
    review text,
    rating int check(rating >=0 and rating <= 10),
    unique(user_id, movie_id)    

);

create table wtw_review_vote (
    id serial primary key,
    user_id int references wtw_user(id),
    review_id int references wtw_review(id),
    isUpvote bool not null,
    unique (user_id, review_id)
);

create table wtw_comment (
    id serial primary key,
    user_id int references wtw_user(id),
    comment text,
    review_id int references wtw_review(id)
);

create table wtw_watchlist (
    id serial primary key,
    user_id int references wtw_user(id),
    movie_id int references wtw_movie(id),
    unique(user_id, movie_id)    
);

alter table wtw_review add vote_count int default 0;

create or replace function update_votes() returns trigger as '
    declare
        delta int;
        review_id int;
    begin
        delta := 0;
        if (tg_op = ''DELETE'') then
            review_id = old.review_id;
            if(old.isupvote) then
                delta := -1;
            else
                delta := 1;
            end if;
        elsif (tg_op = ''UPDATE'') then
            review_id = new.review_id;
            if (old.isupvote <> new.isupvote) then
                if (new.isupvote) then 
                    delta := 2;
                else
                    delta := -2;
                end if;
            end if;
        elsif (tg_op = ''INSERT'') then
            review_id = new.review_id;
            if(new.isupvote) then
                delta := 1;
            else 
                delta := -1;
            end if;
        end if;
        update wtw_review set vote_count = vote_count + delta where id = review_id;

        return null;
    end;
' language 'plpgsql';

create trigger vote_trigger after insert or delete or update on wtw_review_vote
for each row execute procedure update_votes();

create or replace function update_ratings() returns trigger as '
    declare
        movie_id int;
        old_avg real;
        old_count int;
        new_avg real;
        new_count int;
        d int;
    begin
        if (tg_op = ''DELETE'') then
            movie_id = old.movie_id;
            select review_count, avg_rating into old_count, old_avg from wtw_movie where id = movie_id;
            new_count = old_count - 1;
            d := new_count;
            if(new_count = 0) then d := 1; end if;
            new_avg = ((old_count * old_avg) - old.rating)/d;            
        elsif (tg_op = ''UPDATE'') then
            movie_id = new.movie_id;
            select review_count, avg_rating into old_count, old_avg from wtw_movie where id = movie_id;
            new_count = old_count;
            d := new_count;
            if(new_count = 0) then d := 1; end if;
            new_avg = ((old_count * old_avg) - old.rating + new.rating)/d;
        elsif (tg_op = ''INSERT'') then
            movie_id = new.movie_id;
            select review_count, avg_rating into old_count, old_avg from wtw_movie where id = movie_id;
            new_count = old_count + 1;
            d := new_count;
            if(new_count = 0) then d := 1; end if;
            new_avg = ((old_count * old_avg) + new.rating)/d;
        end if;
        update wtw_movie set avg_rating = new_avg, review_count = new_count where id = movie_id;

        return null;
    end;
' language 'plpgsql';


create trigger rating_trigger after insert or delete or update on wtw_review
for each row execute procedure update_ratings();

alter table wtw_comment add posted_at timestamp default current_timestamp;
alter table wtw_review add posted_at timestamp default current_timestamp;

ALTER TABLE wtw_comment
DROP CONSTRAINT wtw_comment_review_id_fkey,
ADD CONSTRAINT wtw_comment_review_id_fkey
   FOREIGN KEY (review_id)
   REFERENCES wtw_review(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_review_vote
DROP CONSTRAINT wtw_review_vote_review_id_fkey,
ADD CONSTRAINT wtw_review_vote_review_id_fkey
   FOREIGN KEY (review_id)
   REFERENCES wtw_review(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_movie_genre
DROP CONSTRAINT wtw_movie_genre_movie_id_fkey,
ADD CONSTRAINT wtw_movie_genre_movie_id_fkey
   FOREIGN KEY (movie_id)
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_movie_language
DROP CONSTRAINT wtw_movie_language_movie_id_fkey,
ADD CONSTRAINT wtw_movie_language_movie_id_fkey
   FOREIGN KEY (movie_id)
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_review
DROP CONSTRAINT wtw_review_movie_id_fkey,
ADD CONSTRAINT wtw_review_movie_id_fkey
   FOREIGN KEY (movie_id) 
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_watchlist
DROP CONSTRAINT wtw_watchlist_movie_id_fkey,
ADD CONSTRAINT wtw_watchlist_movie_id_fkey
   FOREIGN KEY (movie_id) 
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

CREATE OR REPLACE FUNCTION register_vote (user_id int, review_id int, vote boolean) 
RETURNS int as
'DECLARE
      v boolean := false;
   BEGIN
      IF EXISTS(SELECT * FROM wtw_review_vote wr where wr.user_id=$1 and wr.review_id=$2) THEN
        SELECT isupvote into v from wtw_review_vote as wr where wr.user_id=$1 and wr.review_id=$2;
        IF v=$3 THEN
            DELETE FROM wtw_review_vote wr WHERE wr.user_id=$1 and wr.review_id=$2;
        ELSE
            UPDATE wtw_review_vote wr SET isupvote=NOT isupvote where wr.user_id=$1 and wr.review_id=$2;
        END IF;
      ELSE        
        INSERT INTO wtw_review_vote(user_id,review_id,isupvote) VALUES($1,$2,$3);
      END IF;
    return 0;
END;'LANGUAGE plpgsql;

-- not a permanent soln -> update wtw_movie set movie_poster_name = 'default_poster.svg';
alter table wtw_movie drop COLUMN movie_poster_name;
alter table wtw_movie add movie_poster_name varchar(100) default 'default_poster.svg';

alter table wtw_movie add imdb_id varchar(100);

create table wtw_actor (
    id serial primary key,
    actor varchar(100) not null unique
);

create table wtw_movie_actor (
    id serial primary key,
    movie_id int references wtw_movie(id),
    actor_id int references wtw_actor(id)
);

create table wtw_director(
    id serial primary key,
    director varchar(100)
);

create table wtw_movie_director(
    id serial primary key,
    movie_id int references wtw_movie(id),
    director_id int references wtw_director(id)
);

ALTER TABLE wtw_movie_actor
DROP CONSTRAINT wtw_movie_actor_movie_id_fkey,
ADD CONSTRAINT wtw_movie_actor_movie_id_fkey
   FOREIGN KEY (movie_id)
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

ALTER TABLE wtw_movie_director
DROP CONSTRAINT wtw_movie_director_movie_id_fkey,
ADD CONSTRAINT wtw_movie_director_movie_id_fkey
   FOREIGN KEY (movie_id)
   REFERENCES wtw_movie(id)
   ON DELETE CASCADE;

create table wtw_follow
(
    user_id int references wtw_user(id),
    following_id int references wtw_user(id),
    primary key(user_id, following_id)
);

create table wtw_notifications
(   
    id serial primary key,
    sender_id int references wtw_user(id),
    recipient_id int references wtw_user(id),
    parameter_id int,
    notification_type varchar(10) not null,
    created_at timestamp default current_timestamp,
    unread bool not null default true,
);

create or replace function create_notification() returns trigger as '
    declare
        recipient_id int;
        sender_id int;
        parameter_id int;
    begin
        sender_id = NEW.user_id;
        if TG_TABLE_NAME = ''wtw_review'' then
            parameter_id = NEW.id;
            for recipient_id in select user_id from wtw_follow where following_id = new.user_id
            loop
                insert into wtw_notifications(sender_id,recipient_id,parameter_id,notification_type) values(new.user_id,recipient_id,new.id,''review'');
            end loop;
        end if;

        if TG_TABLE_NAME = ''wtw_follow'' then
            select following_id into recipient_id from wtw_follow where user_id  = new.user_id;
            insert into wtw_notifications(sender_id,recipient_id,parameter_id,notification_type) values(new.user_id,recipient_id,new.user_id,''follow'');
        end if;

        if TG_TABLE_NAME = ''wtw_review_vote'' then
            select wr.user_id into recipient_id from wtw_review wr,wtw_review_vote wrv
                where wrv.review_id=wr.id  and review_id=new.review_id;
            insert into wtw_notifications(sender_id,recipient_id,parameter_id,notification_type) values(new.user_id,recipient_id,new.review_id,''vote''); 
        end if;

        if TG_TABLE_NAME = ''wtw_comment'' then
            select wr.user_id into recipient_id from wtw_comment wc, wtw_review wr
                where wc.review_id=wr.id and review_id=new.review_id; 
                insert into wtw_notifications(sender_id,recipient_id,parameter_id,notification_type) values(new.user_id,recipient_id,new.review_id,''comment'');
        end if;

        return null;
    end;
' language 'plpgsql';


create trigger review_notification_trigger after insert on wtw_review
for each row execute procedure create_notification();


create trigger follow_notification_trigger after insert on wtw_follow
for each row execute procedure create_notification();


create trigger vote_notification_trigger after insert on wtw_review_vote
for each row execute procedure create_notification();

create trigger comment_notification_trigger after insert on wtw_comment
for each row execute procedure create_notification();

alter table wtw_movie drop COLUMN movie_poster_name;
alter table wtw_movie add movie_poster_name varchar(100) default '/images/default_poster.svg';
alter table wtw_movie add show_poster_from_imdb bool default 'true';

create table wtw_custom_list (
    id serial primary key,
    name text not null,
    user_id int references wtw_user(id),
    is_public boolean default 'false',
    star_count int default 0,
    movie_count int default 0
);

create table wtw_custom_list_item (
    id serial primary key,
    custom_list_id int references wtw_custom_list(id),
    movie_id int references wtw_movie(id) on delete CASCADE,
    unique(custom_list_id, movie_id)
);

ALTER TABLE wtw_custom_list_item
DROP CONSTRAINT wtw_custom_list_item_custom_list_id_fkey,
ADD CONSTRAINT wtw_custom_list_item_custom_list_id_fkey
   FOREIGN KEY (custom_list_id)
   REFERENCES wtw_custom_list(id)
   ON DELETE CASCADE;

create table wtw_custom_list_star (
    id serial primary key,
    custom_list_id int references wtw_custom_list(id) on delete CASCADE,
    user_id int references wtw_user(id)
);

alter table wtw_custom_list_star add constraint unique_star unique(user_id, custom_list_id);

create or replace function update_star_count() returns trigger as '
    declare
        delta int;
        custom_list_id int;
    begin
        delta := 0;
        if (tg_op = ''DELETE'') then
            custom_list_id = old.custom_list_id;
            delta := -1;
        else
            custom_list_id = new.custom_list_id;
            delta := 1; 
        end if;
        update wtw_custom_list set star_count = star_count + delta where id = custom_list_id;
        return null;
    end;
' language 'plpgsql';

create trigger star_count_trigger after insert or delete on wtw_custom_list_star
for each row execute procedure update_star_count();

create or replace function update_movie_count() returns trigger as '
    declare
        delta int;
        custom_list_id int;
    begin
        delta := 0;
        if (tg_op = ''DELETE'') then
            custom_list_id = old.custom_list_id;
            delta := -1;
        else
            custom_list_id = new.custom_list_id;
            delta := 1; 
        end if;
        update wtw_custom_list set movie_count = movie_count + delta where id = custom_list_id;
        return null;
    end;
' language 'plpgsql';

create trigger movie_count_trigger after insert or delete on wtw_custom_list_item
for each row execute procedure update_movie_count();

create table wtw_report_resource(
    resource_type varchar(15) primary key
);

insert into wtw_report_resource values ('REVIEW'),('COMMENT'),('MOVIE'),('USER');

create table wtw_report_reason(
    id serial primary key,
    resource_type varchar(15) references wtw_report_resource(resource_type),
    report_reason text
);

insert into wtw_report_reason(resource_type, report_reason) values
    ('REVIEW', 'Spam'),
    ('REVIEW', 'Use of Inappropriate Language'),
    ('REVIEW', 'Harassment'),
    ('REVIEW', 'Use of Hate Speech'),
    ('REVIEW', 'Encourages Violence / Self Harm'),
    ('COMMENT', 'Spam'),
    ('COMMENT', 'Use of Inappropriate Language'),
    ('COMMENT', 'Harassment'),
    ('COMMENT', 'Use of Hate Speech'),
    ('COMMENT', 'Encourages Violence / Self Harm'),
    ('MOVIE', 'Incorrect Metadata - Title and/or Release Year'),
    ('MOVIE', 'Incorrect Metadata - Summary'),
    ('MOVIE', 'Incorrect Metadata - Cast/Crew Info'),
    ('MOVIE', 'Incorrect Metadata - Movie Poster'),
    ('MOVIE', 'Incorrect Metadata - Language/Genre'),
    ('USER', 'Inappropriate Conduct');

create table wtw_reports(
    id serial primary key,
    user_id int references wtw_user(id),
    resource_id int not null,
    resource_type varchar(15) references wtw_report_resource(resource_type),
    reason_id int references wtw_report_reason(id),
    report_status varchar(10) check (report_status in ('UNSEEN','IGNORED','COMPLETED')) default 'UNSEEN',
    reported_at timestamp default current_timestamp
);

-- insert into wtw_reports(user_id, resource_id, resource_type, reason_id, report_status) values
--  (2, 10, 'REVIEW', 1, 'DONE');

--  insert into wtw_reports(user_id, resource_id, resource_type, reason_id, report_status) values
--  (3, 13, 'COMMENT', 7, 'UNSEEN');

--  insert into wtw_reports(user_id, resource_id, resource_type, reason_id, report_status) values
--  (3, 13, 'MOVIE', 11, 'UNSEEN');

--   insert into wtw_reports(user_id, resource_id, resource_type, reason_id, report_status) values
--  (3, 5, 'USER', 16, 'UNSEEN');


DROP TRIGGER IF EXISTS review_notification_trigger ON wtw_review;
DROP TRIGGER IF EXISTS follow_notification_trigger ON wtw_follow;
DROP TRIGGER IF EXISTS vote_notification_trigger ON wtw_review_vote;
DROP TRIGGER IF EXISTS comment_notification_trigger ON wtw_comment;

DROP FUNCTION IF EXISTS create_notification;
drop table IF EXISTS wtw_notifications;

create table wtw_notifications (
    id serial primary key,
    title text not null,
    body text,
    uri text,
    category int default 0,    
    recipient_id int references wtw_user(id),
    created_at timestamp default current_timestamp,
    unread bool not null default true
);

create or replace function fn_follow() returns trigger as '
    declare
        username varchar(20);
    begin
        select wtw_user.username into username from wtw_user where id = new.user_id;
        insert into wtw_notifications (title, body, uri, recipient_id) values (''New Follower'', username || '' followed you'', ''/account.php?id='' || new.user_id, new.following_id);
        return null;
    end;
' language 'plpgsql';

create trigger follow_notification_trigger after insert on wtw_follow
for each row execute procedure fn_follow();

create or replace function fn_comment() returns trigger as '
    declare
        recipient_id int;
        username varchar(20);
        m_id int;
        movie_name varchar(200);
    begin
        select wtw_user.username into username from wtw_user where id = new.user_id;
        select user_id, movie_id into recipient_id, m_id from wtw_review where id = new.review_id;
        select wtw_movie.movie_name into movie_name from wtw_movie where id = m_id;
        insert into wtw_notifications (title, body, uri, recipient_id) values (
            username || '' commented on your review of '' || movie_name,
            new.comment,
            ''/review_page.php?id=''||new.review_id||''#comment''||new.id,
            recipient_id
        );
        return null;
    end;
' language 'plpgsql';

create trigger comment_notification_trigger after insert on wtw_comment
for each row execute procedure fn_comment();

create table wtw_disable_user (
    user_id int references wtw_user(id) primary key,
    reason text,
    disabled_at timestamp default current_timestamp
);

create or replace function fn_disable_user() returns trigger as '
    declare

    begin
        insert into wtw_notifications (title, body, recipient_id, category) values (''You are disabled by admin'', new.reason, new.user_id, 2);
        return null;
    end;
' language 'plpgsql';

create trigger disable_user_notification_trigger after insert on wtw_disable_user
for each row execute procedure fn_disable_user();