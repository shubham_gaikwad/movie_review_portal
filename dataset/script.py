# imdb_title_id,title,original_title,year,date_published,genre,duration,country,language,director,writer,production_company,actors,description,avg_vote,votes,budget,usa_gross_income,worlwide_gross_income,metascore,reviews_from_users,reviews_from_critics
from bisect import bisect_left
import csv


def BinarySearch(a, x):
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    else:
        return -1


class Movie(object):
    def __init__(self, arr):
        self.title = arr[1].replace("'", "\\'")
        self.original_title = arr[2].replace("'", "\\'")
        self.year = arr[3]
        self.genre = list(map(lambda s: s.replace(',', '').replace(
            ' ', '').lower().capitalize(), str(arr[5]).split(sep=',')))
        self.language = list(set(list(map(lambda s: s.replace(
            ',', '').lower().lstrip().capitalize(), str(arr[8]).split(sep=',')))))
        self.actors = list(
            map(lambda s: s.lower().lstrip().title().replace("'", "\\'"), str(arr[12]).split(sep=',')))
        self.director = list(
            map(lambda s: s.lower().lstrip().title().replace("'", "\\'"), str(arr[9]).split(sep=',')))
        self.description = arr[13].replace("'", "\\'")
        self.imdb_id = arr[0]


def read_movies(filename):
    movies = []
    print('Reading Movies from {}'.format(filename))
    with open(filename, 'r') as file:
        reader = csv.reader(file)
        fields = next(reader)
        for m in reader:
            if(m[5] != '' and m[8] != ''):
                movies.append(Movie(m))
    print('Done, {} movies found\n'.format(len(movies)))
    return movies


def read_genres(movies):
    print('Reading Genres')
    genres = set()
    for m in movies:
        for g in m.genre:
            genres.add(g)
    print('Done, {} genres found\n'.format(len(genres)))
    return sorted(genres)


def read_languages(movies):
    print('Reading Languages')
    languages = set()
    for m in movies:
        for l in m.language:
            languages.add(l)
    print('Done, {} languages found\n'.format(len(languages)))
    return sorted(languages)


def read_actors(movies):
    print('Reading Actors')
    actors = set()
    for m in movies:
        for a in m.actors:
            actors.add(a)
    print('Done, {} actors found\n'.format(len(actors)))
    return sorted(actors)


def read_directors(movies):
    print('Reading Directors')
    directors = set()
    for m in movies:
        for d in m.director:
            directors.add(d)
    print('Done, {} directors found\n'.format(len(directors)))
    return sorted(directors)


class DBGenerator(object):
    def __init__(self, movies, genres, languages, actors, directors):
        self.movies = movies
        self.genres = genres
        self.languages = languages
        self.actors = actors
        self.directors = directors

    def reference_values(self):
        self.reference_genres()
        self.reference_languages()
        self.reference_actors()
        self.reference_directors()

    def reference_genres(self):
        print('Referencing genres')
        for m in self.movies:
            m.genre = list(
                map(lambda x: BinarySearch(self.genres, x) + 1, m.genre))
        print('Done\n')

    def reference_languages(self):
        print('Referencing languages')
        for m in self.movies:
            m.language = list(map(lambda x: BinarySearch(
                self.languages, x) + 1, m.language))
        print('Done\n')

    def reference_actors(self):
        print('Referencing actors')
        for m in self.movies:
            m.actors = list(
                map(lambda x: BinarySearch(self.actors, x) + 1, m.actors))
        print('Done\n')

    def reference_directors(self):
        print('Referencing directors')
        for m in self.movies:
            m.director = list(
                map(lambda x: BinarySearch(self.directors, x) + 1, m.director))
        print('Done\n')

    def generate_movies(self, filename):
        print('Writing Movies')
        values = []
        for m in self.movies:
            values.append("  ({}, E'{}', E'{}', E'{}', E'{}')".format(
                len(values)+1, m.original_title, m.year, m.description, m.imdb_id))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_movie (id, movie_name, release_year, summary, imdb_id) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_movie_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_genres(self, filename):
        print('Writing Genres')
        values = []
        for g in self.genres:
            values.append("  ({}, '{}')".format(
                len(values)+1, g))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_genre (id, genre_name) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_genre_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_languages(self, filename):
        print('Writing Languages')
        values = []
        for l in self.languages:
            values.append("  ({}, '{}')".format(
                len(values)+1, l))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_language (id, language_name) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_language_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_actors(self, filename):
        print('Writing Actors')
        values = []
        for a in self.actors:
            values.append("  ({}, E'{}')".format(
                len(values)+1, a))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_actor (id, actor) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_actor_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_directors(self, filename):
        print('Writing Directors')
        values = []
        for a in self.directors:
            values.append("  ({}, E'{}')".format(
                len(values)+1, a))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_director (id, director) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_director_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_movie_genre(self, filename):
        print('Writing Movie-Genre')
        values = []
        for m in range(len(self.movies)):
            movie = self.movies[m]
            for g in movie.genre:
                values.append("  ({}, {}, {})".format(
                    len(values)+1, m+1, g))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_movie_genre (id, movie_id, genre_id) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_movie_genre_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_movie_language(self, filename):
        print('Writing Movie-Language')
        values = []
        for m in range(len(self.movies)):
            movie = self.movies[m]
            for l in movie.language:
                values.append("\t({}, {}, {})".format(
                    len(values)+1, m+1, l))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_movie_language (id, movie_id, language_id) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_movie_language_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_movie_actor(self, filename):
        print('Writing Movie-Actor')
        values = []
        for m in range(len(self.movies)):
            movie = self.movies[m]
            for a in movie.actors:
                values.append("  ({}, {}, {})".format(
                    len(values)+1, m+1, a))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_movie_actor (id, movie_id, actor_id) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_movie_actor_id_seq\', {}, true);'.format(count))
            print('Done\n')

    def generate_movie_director(self, filename):
        print('Writing Movie-Director')
        values = []
        for m in range(len(self.movies)):
            movie = self.movies[m]
            for d in movie.director:
                values.append("  ({}, {}, {})".format(
                    len(values)+1, m+1, d))
        with open(filename, 'w') as file:
            file.write(
                'insert into wtw_movie_director (id, movie_id, director_id) values\n')
            count = len(values)
            for i in range(count):
                v = values[i]
                file.write(v)
                if i < count - 1:
                    file.write(',')
                else:
                    file.write(';')
                file.write('\n')
            file.write(
                'SELECT setval(\'wtw_movie_director_id_seq\', {}, true);'.format(count))
            print('Done\n')


def main():
    movies = read_movies('movies.csv')
    genres = read_genres(movies)
    languages = read_languages(movies)
    actors = read_actors(movies)
    directors = read_directors(movies)
    db = DBGenerator(movies=movies, genres=genres,
                     languages=languages, actors=actors, directors=directors)
    db.reference_values()
    db.generate_movies('m.wtw')
    db.generate_genres('g.wtw')
    db.generate_languages('l.wtw')
    db.generate_actors('a.wtw')
    db.generate_directors('d.wtw')
    db.generate_movie_genre('m_g.wtw')
    db.generate_movie_language('m_l.wtw')
    db.generate_movie_actor('m_a.wtw')
    db.generate_movie_director('m_d.wtw')


if __name__ == "__main__":
    main()
